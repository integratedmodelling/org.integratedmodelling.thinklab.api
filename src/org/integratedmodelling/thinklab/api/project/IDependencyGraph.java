package org.integratedmodelling.thinklab.api.project;

import java.util.Collection;

import org.integratedmodelling.thinklab.api.modelling.INamespace;

/**
 * An object dedicated to inquiring about namespace dependencies. The IProjectManager keeps one
 * up to date.
 * 
 * @author Ferd
 *
 */
public interface IDependencyGraph {

    /**
     * Return all the namespaces that depend on the passed one, recursively.
     *
     * @param ns
     * @return
     */
    Collection<INamespace> getDependents(INamespace ns);

    /**
     * Return all the namespaces that the passed one depends on, recursively.
     * 
     * @param ns
     * @return
     */
    Collection<INamespace> getPrerequisites(INamespace ns);

}
