package org.integratedmodelling.thinklab.api.project;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.connection.IConnection;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.plugin.IThinklabPlugin;

/**
 * Thinklab projects are plug-in packages that provide Thinklab resources such as models, annotations etc. 
 * This interface is shared by the client and server side. Projects at the server side will need more
 * methods to manage their lifetime in Thinklab.
 * @author  Ferd
 */
public interface IProject extends IThinklabPlugin {

    public static final String THINKLAB_META_INF = "META-INF";
    public static final String THINKLAB_PROPERTIES_FILE = "thinklab.properties";

    // properties for META-INF/thinklab.properties file. May also have metadata
    // according to IMetadata fields.
    public static final String SOURCE_FOLDER_PROPERTY = "thinklab.source.folder";
    public static final String ONTOLOGY_NAMESPACE_PREFIX_PROPERTY = "thinklab.ontology.prefix";
    public static final String PREREQUISITES_PROPERTY = "thinklab.prerequisites";
    public static final String VERSION_PROPERTY = "project.version";
    public static final String DEFAULT_NAMESPACE_PREFIX = "http://www.integratedmodelling.org/ns";

    /**
     * Return all the namespaces defined in the project.
     * 
     * @return
     */
    public Collection<INamespace> getNamespaces();

    /**
     * Source folders are scanned at startup and monitored during development. The location of
     * source folders is in the SOURCE_FOLDER_PROPERTY of the thinklab properties. There is
     * one source folder per project.
     * 
     * @return the path of the source directory, relative to the project workspace.
     */
    public String getSourceDirectory();

    /**
     * Return all permanent data connections configured in the project. This is a user
     * feature, not directly used by models.
     * 
     * @return
     */
    public Collection<IConnection> getConnections();

    /**
     * Any ontologies in the source folder must have a URL that matches the project's 
     * ontology namespace prefix (from properties, defaulting to http://www.integratedmodelling.org/ns)
     * followed by the path corresponding to the ontology namespace and file name.
     * 
     * @return
     */
    public String getOntologyNamespacePrefix();

    /**
     * Find the resource file that defines the passed namespace. If not
     * found, return null.
     * 
     * @param namespace
     * @return
     */
    public File findResourceForNamespace(String namespace);

    /**
     * Find a namespace either in this project or in any of the 
     * prerequisites, using the ProjectManager to load it if necessary. Return
     * null if nothing is found. 
     * 
     * @param namespace
     * @return
     * @throws ThinklabException for errors during load
     */
    public INamespace findNamespaceForImport(String namespace, IModelResolver resolver, IMonitor monitor)
            throws ThinklabException;

    /**
     * Get all projects we depend on. These should be ordered in load order.
     * 
     * @return
     * @throws ThinklabException 
     */
    public abstract List<IProject> getPrerequisites() throws ThinklabException;

    /**
     * Return when this was last modified, so that we can load efficiently. This should
     * return the modification time of the newest resource.
     * 
     * @return
     */
    public long getLastModificationTime();

    /**
     * Return true if this provides a definition for the named namespace. Must be
     * able to answer without loading anything.
     * 
     * @param namespaceId
     * @return
     */
    boolean providesNamespace(String namespaceId);

    /**
     * Return the name of each top-level resource folder in the project that
     * is not one of the automatically managed ones.
     * 
     * @return
     */
    public abstract List<String> getUserResourceFolders();

    /**
     * Return true if any of the resources in the project has caused compilation errors. If
     * this is true, the project should not be used.
     * 
     * @return
     */
    public boolean hasErrors();

    public boolean hasWarnings();

    /**
     * Return all the script files in the project. Those should be in their own directory.
     * 
     * @return
     */
    public Collection<File> getScripts();
}
