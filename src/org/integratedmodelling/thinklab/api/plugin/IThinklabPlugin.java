package org.integratedmodelling.thinklab.api.plugin;

import java.io.File;
import java.util.Properties;

import org.integratedmodelling.thinklab.Version;

/**
 * Simple interface for a Thinklab plugin. Should be easy to implement on top of
 * any plugin infrastructure, simple or complex, or even without one. Thinklab
 * plugins do not expect to be hot-swappable or anything fancy. 
 * 
 * @author Ferd
 *
 */
public interface IThinklabPlugin {

    /**
     * Plugins have a name.
     * 
     * @return
     */
    public String getId();

    /**
     * Find a resource file or directory in the plugin. Extract it to a scratch
     * area if necessary so it is a File that can be copied and 
     * used locally. Expected to be read only. Return null if not found.
     * 
     * @param resource
     * @return
     */
    public File findResource(String resource);

    /**
     * Installation directory. It needs to be non-null even if the plugin doesn't have resources.
     * 
     * @return
     */
    public File getLoadPath();

    /**
     * Also must not return null.
     * 
     * @return
     */
    public Properties getProperties();

    /**
     * 
     * @return
     */
    public Version getVersion();

    // /**
    // * True if unload() can be called.
    // *
    // * @return
    // */
    // public boolean isLoaded();

}
