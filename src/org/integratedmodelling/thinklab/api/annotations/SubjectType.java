package org.integratedmodelling.thinklab.api.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Associates a Java Agent subclass to a specific concept, so that the class will be created to
 * incarnate an agent with that semantics. Works using the reasoner, so that subclasses of that 
 * concept will be matched to this agent.
 * 
 * @author Ferd
 * @see org.integratedmodelling.thinklab.api.annotations.Property
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface SubjectType {
    public String value();
}
