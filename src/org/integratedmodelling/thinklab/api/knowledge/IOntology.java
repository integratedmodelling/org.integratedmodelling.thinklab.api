/**
 * IOntology.java
 * ----------------------------------------------------------------------------------
 * 
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of Thinklab.
 * 
 * Thinklab is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thinklab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * ----------------------------------------------------------------------------------
 * 
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @author    Ioannis N. Athanasiadis (ioannis@athanasiadis.info)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.api.knowledge;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Ontologies are not first-class objects in Thinklab. This interface isn't deprecated at the moment, but
 * it may go away any time. All retrieval of concepts should be done through INamespace.
 */
public interface IOntology extends IResource {

    /**
     * Iterate over all concepts
     * @return an iterator over all the concepts contained in the ontology. 
     */
    Collection<IConcept> getConcepts();

    /**
     * Iterate over all properties
     * @return an iterator over all the properties contained in the ontology. 
     */
    Collection<IProperty> getProperties();

    /**
     * Return a concept, or null if not found.
     * @param ID the concept's ID
     * @return the concept or null
     */
    IConcept getConcept(String ID);
    
    /**
     * Return the flags associated with the concept, an integer with implementation-dependent usage.
     * Note that this should return 0 for any concept ID that is not in the ontology. Retrieval of
     * these flags should be as fast as possible, as they are meant to avoid reasoning and annotation
     * processing for screening concepts in indexing.
     * 
     * @param ID
     * @return
     */
    int getConceptOptions(String ID);

    /**
     * Return a property, or null if not found.
     * @param ID the property's ID
     * @return the property or null
     */
    IProperty getProperty(String ID);

    /**
     */
    String getURI();

    /**
     * Write the ontology to the passed physical location.
     * 
     * @param uri
     * @throws ThinklabException 
     */
    boolean write(String uri) throws ThinklabException;

    /**
     * Define the ontology from a collection of axioms. Must work incrementally.
     * @param axioms
     * @return a list of error messages if any happened. Should not throw exceptions.
     * 
     * TODO provide a quicker define(Axiom ... axioms) 
     * 
     * @throws ThinklabException
     */
    Collection<String> define(Collection<IAxiom> axioms);

    /**
     * Return the number of (named, useful) concepts, hopefully quickly. 
     * 
     * @return
     */
    int getConceptCount();

    /**
     * Return the number of (named, useful) properties, hopefully quickly.
     * 
     * @return
     */
    int getPropertyCount();
}
