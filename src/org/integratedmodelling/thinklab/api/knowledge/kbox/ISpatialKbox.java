package org.integratedmodelling.thinklab.api.knowledge.kbox;

import java.util.List;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;

/**
 * Additional functions that a space-aware KBox can provide. All these are accessible through
 * standard queries, but the specialized methods allow more efficient operation and a simpler
 * API usage.
 * 
 * @author Ferd
 *
 */
public interface ISpatialKbox {

    /**
     * Return objects closest to a location, in decreasing distance order.
     * 
     * @param type of the object we want
     * @param locationProperty the property that locates the object in space
     * @param location the location 
     * @param distanceInDegrees 
     * @return
     */
    List<ISemanticObject<?>> closestTo(IConcept type, IProperty locationProperty, Object location,
            double distanceInDegrees);

}
