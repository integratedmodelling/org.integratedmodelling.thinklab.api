//package org.integratedmodelling.thinklab.api.knowledge.kbox;
//
//import org.integratedmodelling.thinklab.api.metadata.IMetadata;
//
///**
// * Criteria used to rank candidate observations for use in dependency resolution. The object contains the
// * criteria and implements an appropriate ranking mechanism to produce a score for another object based on its
// * metadata and possibly the scale it's in and the subject that provides its context.
// * 
// * @author Ferd
// * @deprecated use model prioritizer and resolution context
// */
//public interface IResolutionCriteria extends IMetadata {
//
//    public double getRank(IMetadata data);
//
//}
