package org.integratedmodelling.thinklab.api.knowledge;

public interface ICondition extends IExpression {

    /**
     * Fairly stupid, but solves lots of problems. If this is called with a true
     * argument, it should carry a negated flag that will be checked when it's used
     * as a condition.
     * 
     * @param negate
     */
    public abstract void setNegated(boolean negate);

    public abstract boolean isNegated();

}
