/**
 * IConcept.java
 * ----------------------------------------------------------------------------------
 * 
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of Thinklab.
 * 
 * Thinklab is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thinklab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * ----------------------------------------------------------------------------------
 * 
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @author    Ioannis N. Athanasiadis (ioannis@athanasiadis.info)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.api.knowledge;

import java.util.Collection;
import java.util.Set;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.query.IQuery;

/**
 * 
 * @author  Ferdinando Villa
 */
public interface IConcept extends IKnowledge {

	/*
	 * flag values for concept created by thinklab or by the user. If a concept
	 * has no flags, it must come from external ontologies.
	 */
	public static final int CONCEPT_IS_DECLARED = 0x0001;
    public static final int CONCEPT_IS_COMPUTED = 0x0002;
	public static final int CONCEPT_IS_LOCAL = 0x0004;

	/**
     * Return a collection of all the direct parent classes.
     * @return
     */
    Collection<IConcept> getParents();

    /**
     * Return a collection of all direct and indirect parent classes. Should use a reasoner if
     * installed, and only follow transitive superclass relationships if not.
     * @return
     */
    Collection<IConcept> getAllParents();

    /**
     * Return a collection of all direct subclasses.
     * @return all child concepts.
     */
    Collection<IConcept> getChildren();

    /**
     * Return all properties that are  defined as having this in their domain
     * subproperties.
     * @return
     */
    Collection<IProperty> getProperties();

    /**
     * It includes all properties inherited by its superclasses, or having undefined domains
     * @return
     */
    Collection<IProperty> getAllProperties();

    /**
     * Return the range of the passed property in the context of this concept, considering
     * restrictions.
     * @return
     * @throws ThinklabException 
     */
    Collection<IConcept> getPropertyRange(IProperty property) throws ThinklabException;

    /**
     * Get the value that the passed data property is restricted to in this concept, or null if there
     * is no restriction. 
     * 
     * @param property
     * @return
     */
    Object getValueOf(IProperty property) throws ThinklabException;

    /**
     * The notion of an abstract concept is important where we create instances that have implementations. 
     * @return true if concept is abstract (no instances can be created).
     */
    boolean isAbstract();

    /**
     * Return the (only) parent class, or throw an unchecked exception if there's more than one parent.
     */
    IConcept getParent();

    /** get the number of properties for this type */
    int getPropertiesCount(String property);

    IConcept getLeastGeneralCommonConcept(IConcept c);

    IQuery getDefinition();

    /**
     * Return the full set of concepts that are subsumed by this concept, using
     * whatever reasoning strategy is implemented or configured in.
     * 
     * This is used quite a bit in kbox queries, so it pays to make it
     * fast and/or cache results.
     * 
     * @return
     */
    Set<IConcept> getSemanticClosure();

    /**
     * Return min,max cardinality of property when applied to this concept. -1 on either end
     * indicates no cardinality given.
     * 
     * @param property
     * @return
     */
    int[] getCardinality(IProperty property);

    /**
     * Called on an abstract concept to retrieve all the children that are concrete and disjoint, meaning
     * those for which is(this) is true but both a.is(b) and b.is(a) is false.
     * 
     * @return
     */
    Collection<IConcept> getDisjointConcreteChildren();

}
