package org.integratedmodelling.thinklab.api.knowledge;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;

/**
 * An authority processes an ID into an identity. It must have a unique name and correspond to
 * the "official" view of a controlled vocabulary or repository of unique IDs that correspond
 * to unambiguous identities. The Thinklab counterpart is a trait concept that can be used to
 * identify any fundamental observable.
 * 
 * @author Ferd
 *
 */
public interface IAuthority {

    String getAuthorityId();

    IConcept getIdentity(String id) throws ThinklabValidationException;

    List<IMetadata> search(String query);

    /**
     * Return the concept corresponding to the passed observable identified as the passed identity coming
     * from getIdentity(). Provided so that authorities can validate the observable type.
     * 
     * @param observable
     * @return
     * @throws ThinklabValidationException
     */
    IConcept identify(IConcept observable, IConcept identity) throws ThinklabValidationException;

    /**
     * Provide a longish description of what this authority does.
     * @return
     */
    String getDescription();

    /**
     * True if search() is going to be returning anything useful.
     * 
     * @return
     */
    boolean canSearch();
}
