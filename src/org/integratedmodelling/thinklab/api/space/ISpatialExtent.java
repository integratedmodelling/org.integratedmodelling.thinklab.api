package org.integratedmodelling.thinklab.api.space;

import org.integratedmodelling.thinklab.api.modelling.IExtent;

public interface ISpatialExtent extends IExtent {
    @Override
    public ISpatialExtent getExtent(int stateIndex);

    /**
     * TODO currently, all spatial extents are locked into a 2D coordinate system, due to the internal state
     * of SpaceExtent and ShapeValue consisting of ReferencedEnvelope and Geometry objects. This should be loosened to allow for
     * n-dimensional space (1, 2, or 3). (extra dimensions would probably be outside the ISpatialExtent hierarchy)
     */
    public double getMinX();

    public double getMinY();

    public double getMaxX();

    public double getMaxY();
}
