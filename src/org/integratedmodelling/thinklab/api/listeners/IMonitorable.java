package org.integratedmodelling.thinklab.api.listeners;

/**
 * Extends an expression so we can give it a monitor before evaluation.
 * 
 * @author Ferd
 *
 */
public interface IMonitorable {

    void setMonitor(IMonitor monitor);

    IMonitor getMonitor();

}
