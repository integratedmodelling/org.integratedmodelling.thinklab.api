package org.integratedmodelling.thinklab.api.listeners;

import org.integratedmodelling.thinklab.api.project.IProject;

/**
 * Project listener. Register these with the Project Manager.
 * 
 * @author Ferd
 *
 */
public interface IProjectLifecycleListener extends IListener {

    /**
     * 
     * @param project
     */
    public abstract void projectRegistered(IProject project);

    /**
     * 
     * @param project
     */
    public abstract void projectOpened(IProject project);

    /**
     * 
     * @param project
     */
    public abstract void projectClosed(IProject project);

    /**
     * 
     * @param ns
     * @param project
     */
    public abstract void namespaceDeleted(String ns, IProject project);
}
