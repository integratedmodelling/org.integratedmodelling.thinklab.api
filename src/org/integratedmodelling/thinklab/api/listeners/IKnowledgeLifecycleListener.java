package org.integratedmodelling.thinklab.api.listeners;

import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;

public interface IKnowledgeLifecycleListener extends IListener {

    /**
     * Called as soon as definition of a model object is complete.
     *
     * @param object
     */
    public void objectDefined(IModelObject object);

    /**
     * Called when a namespace is first declared, before any objects
     * are read.
     *
     * @param namespace
     */
    public void namespaceDeclared(INamespace namespace);

}
