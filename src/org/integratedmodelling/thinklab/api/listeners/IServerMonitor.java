package org.integratedmodelling.thinklab.api.listeners;

import java.util.List;

import org.integratedmodelling.thinklab.api.metadata.IMetadata;

///**
// * Server listener can be passed to a IServer to monitor server status and
// * task notifications. Meant to be used inside a polling cycle, more as a 
// * monitor than a listener for events. It serves as a client-side peer of 
// * the IMonitor passed to tasks at the server side, and is also notified
// * the general status of the server.
// * 
// * @author Ferd
// * @see org.integratedmodelling.api.runtime.IServer.listen
// */
//public interface IServerMonitor extends IListener {
//
//    /*
//     * status codes for the server
//     */
//    public static final int STOPPED = 0;
//    public static final int BOOTING = 1;
//    public static final int RUNNING = 2;
//    public static final int ERROR = 3;
//
//    /**
//     * Passed the current server status whenever the listener is invoked.
//     * @param notifications 
//     */
//    void notifyStatus(int status, IMetadata metadata, List<INotification> notifications);
//
//}
