package org.integratedmodelling.thinklab.api;

import org.integratedmodelling.thinklab.api.configuration.IConfiguration;
import org.integratedmodelling.thinklab.api.factories.IKnowledgeManager;
import org.integratedmodelling.thinklab.api.factories.IModelManager;
import org.integratedmodelling.thinklab.api.factories.IProjectManager;
import org.integratedmodelling.thinklab.api.runtime.IUser;

/**
 * An object that can be returned to provide a pointer to the necessary factories. Not necessary
 * within individual applications, but it can be used by anything needing thinklab as a central
 * point to access Thinklab. Each implementation can create one of these and publish it as
 * appropriate. For example IResolver can be asked for one of these so that 
 * language parsers can validate concepts, and the thinklab-common library has methods in
 * Env to set and get the IThinklab for common applications.
 * 
 * @author Ferd
 *
 */
public interface IThinklab {

    /**
     * return true if the server is locked for exclusive operations by an administrator.
     * Only a locked server can be used to deploy projects and swapping of namespaces. Lock
     * is established by a lock file in a non-web accessible directory; an embedded personal
     * server is always locked, but a public one can only be locked by
     * a privileged user through a REST call.
     * 
     * @return
     */
    public boolean isLocked();

    /**
     * If locked, return the locking user. Otherwise return null. User may be anonymous only
     * in an embedded server running on localhost.
     * 
     * @return
     */
    public IUser getLockingUser();

    /**
     * Attempt to lock for passed user; return true if successful.
     * 
     * @param user
     * @param force if true, lock is established no matter what (for embedded servers only)
     * @return
     */
    public boolean lock(IUser user, boolean force);

    /**
     * 
     * @return
     */
    public IKnowledgeManager getKnowledgeManager();

    /**
     * 
     * @return
     */
    public IProjectManager getProjectManager();

    /**
     * 
     * @return
     */
    public IConfiguration getConfiguration();

    /**
     * 
     * @return
     */
    public IModelManager getModelManager();

    /**
     * Logging functions - use appropriate logger.
     * 
     * @param string
     */
    public void warn(String string);

    /**
     * Logging functions - use appropriate logger.
     * 
     * @param string
     */
    public void error(String string);

    /**
     * Logging functions - use appropriate logger.
     * 
     * @param string
     */
    public void info(String string);

    boolean unlock();
}
