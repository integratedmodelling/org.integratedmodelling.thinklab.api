package org.integratedmodelling.thinklab.api.lang;

import java.util.List;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;

/**
 * Prototypes define callable functions and their parameters. In Thinklab it works for commands and
 * language functions so far, both of which can return one of these. A prototype implementation should
 * be able to be validated against a call.
 * 
 * @author Ferd
 *
 */
public interface IPrototype {

    /**
     * 
     * @return
     */
    public String getId();

    /**
     * 
     * @return
     */
    public List<String> getMandatoryArgumentNames();

    /**
     * 
     * @return
     */
    public List<String> getOptionalArgumentNames();

    /**
     * 
     * @param argumentName
     * @return
     */
    public IConcept[] getArgumentTypes(String argumentName);

    /**
     * Return the concept of the return type.
     * 
     * @return
     */
    public IConcept[] getReturnTypes();

    /**
     * 
     * @return
     */
    public String getDescription();

    /**
     * Return true for any command that requires the previous establishment of a session.
     * 
     * @return
     */
    public boolean requiresSession();
}
