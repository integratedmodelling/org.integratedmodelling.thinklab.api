package org.integratedmodelling.thinklab.api.lang;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.runtime.annotations.Command;

public interface ICommandManager {

    ICommand makeCommand(String name, Object[] arguments);

    ICommand parseCommand(String line) throws ThinklabException;

    IPrototype processCommandAnnotation(Command command);

}
