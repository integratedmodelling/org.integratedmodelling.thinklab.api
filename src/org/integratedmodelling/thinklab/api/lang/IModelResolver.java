package org.integratedmodelling.thinklab.api.lang;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.collections.NumericInterval;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.data.ITable;
import org.integratedmodelling.thinklab.api.knowledge.IAuthority;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.IClassifier;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.parsing.IConceptDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.INamespaceDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;
import org.integratedmodelling.thinklab.api.project.IProject;

/**
 * A Resolver is used by anything that generates model objects to interface to the parser for whatever
 * language is implemented. Using a resolver is a way to make any language implementation depend only on the
 * API.
 * 
 * @author Ferd
 * 
 */
public interface IModelResolver {

    public static enum ObservableRole {
        MAIN,
        INHERENT,
        RATIO_DENOMINATOR,
        PROPORTION_DENOMINATOR,
        CLASSIFY_BY_TRAIT
    }

    /*
     * the following are keys for concepts that the upper ontology is expected to provide. Concept definitions
     * will be automatically derived from these according to the idiom used to declare them.
     */
    public static final String OBJECT_CONCEPT             = "object";
    public static final String ATTRIBUTE_CONCEPT          = "attribute";
    public static final String REALM_CONCEPT              = "realm";
    public static final String PROCESS_CONCEPT            = "process";
    public static final String QUALITY_CONCEPT            = "quality";
    public static final String QUANTITY_CONCEPT           = "quantity";
    public static final String QUALITY_SPACE_CONCEPT      = "class";
    public static final String IDENTITY_CONCEPT           = "identity";
    public static final String ORDERING_CONCEPT           = "ordering";
    public static final String THING_CONCEPT              = "thing";
    public static final String ENERGY_CONCEPT             = "energy";
    public static final String ENTROPY_CONCEPT            = "entropy";
    public static final String LENGTH_CONCEPT             = "length";
    public static final String MASS_CONCEPT               = "mass";
    public static final String VOLUME_CONCEPT             = "volume";
    public static final String WEIGHT_CONCEPT             = "weight";
    public static final String DURATION_CONCEPT           = "duration";
    public static final String MONETARY_VALUE_CONCEPT     = "money";
    public static final String PREFERENCE_VALUE_CONCEPT   = "priority";
    public static final String ACCELERATION_CONCEPT       = "acceleration";
    public static final String AREA_CONCEPT               = "area";
    public static final String DENSITY_CONCEPT            = "density";
    public static final String ELECTRIC_POTENTIAL_CONCEPT = "electric-potential";
    public static final String CHARGE_CONCEPT             = "charge";
    public static final String RESISTANCE_CONCEPT         = "resistance";
    public static final String RESISTIVITY_CONCEPT        = "resistivity";
    public static final String PRESSURE_CONCEPT           = "pressure";
    public static final String SLOPE_CONCEPT              = "angle";
    public static final String SPEED_CONCEPT              = "velocity";
    public static final String TEMPERATURE_CONCEPT        = "temperature";
    public static final String VISCOSITY_CONCEPT          = "viscosity";
    public static final String AGENT_CONCEPT              = "agent";
    public static final String EVENT_CONCEPT              = "event";
    public static final String DELIBERATIVE_AGENT_CONCEPT = "deliberative-agent";
    public static final String REACTIVE_AGENT_CONCEPT     = "reactive-agent";
    public static final String SOCIAL_AGENT_CONCEPT       = "social-agent";
    public static final String ORGANIZED_AGENT_CONCEPT    = "organized-agent";
    public static final String SUBJECTIVE_SPECIFIER       = "subjective";

    /**
     * the three fundamental identities of anything used in modeling.
     * @deprecated: use the methods in IModelResolver directly, this is way too simplistic now.
     */
    @Deprecated
    public static enum Identity {
        THING,
        QUALITY,
        PROCESS
    }

    /**
     * Return the fundamental identity of a concept. It should not be possible to create anything that is not
     * one of the three above. If this happens in error, return null.
     * 
     * @param c
     * @return
     */
    Identity getIdentity(IConcept c);

    /**
     * Return the core ontology concept corresponding to the passed key - chosen between those above. These
     * are used to define the semantics of specific observables using language keywords instead of explicit
     * derivations.
     * 
     * @param key
     * @return
     */
    IConcept getConceptFor(String key);

    /**
     * This one returns a new "definable" object for the model class passed. This way each implementation can
     * use their own objects and the API remains clean.
     * 
     * @param cls
     * @param monitor a session monitor that we want the object to use for its internal operation, or null.
     * @return
     */
    ILanguageDefinition newLanguageObject(Class<?> cls, IMonitor monitor);

    /**
     * Override to fine-tune error management. If this throws an exception, the parser also will. If not, the
     * return value determines whether parsing continues (true) or not (false).
     * 
     * @param e
     * @return
     */
    boolean onException(Throwable e, int lineNumber);

    /**
     * Override to fine-tune error management. Will stop parsing if false is returned.
     * 
     * @param warning
     * @return
     */
    boolean onWarning(String warning, int lineNumber);

    /**
     * Override to fine-tune error management.
     * 
     * @param info
     * @return
     */
    boolean onInfo(String info, int lineNumber);

    /**
     * Callback invoked as soon as a namespace declaration is parsed. The resolver should contain the
     * namespace it is being used for.
     * 
     * @param namespaceId
     * @param resourceId
     * @param namespace
     */
    void onNamespaceDeclared();

    /**
     * Callback invoked as soon as parsing of a namespace has been completed. The namespace will contain all
     * model objects declared in it and the axioms collected from them.
     * 
     * @param namespace
     * @throws ThinklabException
     */
    void onNamespaceDefined();

    /**
     * Callback invoked at every new model object at main level. Not all the namespace will be defined when
     * this is called.
     * 
     * @param namespace
     * @param ret
     * @throws ThinklabException
     */
    void onModelObjectDefined(IModelObject ret, IMonitor monitor);

    /**
     * Return a valid concept definition for the passed id. Called on namespace resolvers; must create the
     * concept in the ontology if it does not exist already.
     * 
     * @param id
     * @return
     */
    IConceptDefinition requireLocalConcept(String id, int line, int options);

    /**
     * Called when an external concept name (with the :) is identified in a legal place in a model object.
     * Should return a ConceptObject pointing to whatever definition of the import we need, which will be set
     * in the model tree.
     * 
     * @param id
     * @param namespace
     * @param line
     * @return
     */
    IConceptDefinition resolveExternalConcept(String id, int line);

    /**
     * Called when an external property name (with the :) is identified in a legal place in a model object.
     * Should return a PropertyObject pointing to whatever definition of the import we need, which will be set
     * in the model tree.
     * 
     * @param id
     * @param namespace
     * @param line
     * @return
     */
    IPropertyDefinition resolveExternalProperty(String id, int line);

    /**
     * Check if a passed model object ID has been generated by generateId().
     * 
     * @param id
     * @return
     */
    boolean isGeneratedId(String id);

    /**
     * Generate an ID for the passed model object. Only the type of the model object should be checked, as the
     * remaining attributes may not be initialized when this is called. The ID should be recognizable by
     * isGeneratedId().
     * 
     * @param o
     * @return
     */
    String generateId(IModelObject o);

    /**
     * Return the last model object notified to the resolver. Used by interactive applications only. Should
     * not return any object more than once.
     * 
     * @return
     */
    IModelObject getLastProcessedObject();

    /**
     * Return whether the resolver is being used in an interactive session. The parser may allow or disallow
     * some statements in that case.
     * 
     * @return
     */
    boolean isInteractive();

    /**
     * A root resolver that should be asked to resolve a group of namespaces. Project resolvers should be
     * created from it and namespace resolvers from them. Within a context resolver, it is assumed that
     * namespace resources won't change, and therefore each namespace is only parsed once.
     * 
     * @param monitor a monitor to be used during the whole context parsing.
     * 
     * @return
     */
    IModelResolver getContextResolver(IMonitor monitor);

    /**
     * Return a resolver to read the passed project.
     * @param p
     * @return
     */
    IModelResolver getProjectResolver(IProject p);

    /**
     * Return a resolver for parsing the passed namespace, part of the project this resolver was created for.
     * This one must check that the resource is OK, that the namespace ID and the resource ID match if
     * necessary, prepare to return a valid (empty) namespace when getNamespace() is called, and a valid
     * InputStream for the resource when openStream() is called. Resource checks should be done here - if this
     * exits successfully, openStream should not fail.
     * 
     * For now resource may be null - flagging an interactive namespace that will be defined incrementally
     * without an input stream.
     * 
     * @param namespace
     * @param resource
     * @return
     */
    IModelResolver getNamespaceResolver(String namespace, String resource);

    /**
     * Return the INamespaceDefinition corresponding to the specs given when this resolver was created using
     * getNamespaceResolver(). Must not be null. It will not be called on a resolver not returned by
     * getNamespaceResolver().
     * 
     * @return
     */
    INamespaceDefinition getNamespace();

    /**
     * Return an open InputStream corresponding to the resource given at creation by getNamespaceResolver().
     * It will not be called on a resolver not returned by getNamespaceResolver() or on a namespace whose
     * isInteractive() returns true.
     * 
     * @return
     */
    InputStream openStream();

    /**
     * Return a previously defined namespace from the context namespace catalog, or null if not seen before.
     * It is called to resolve imports, so all projects imported by the currently parsing project should have
     * been parsed when this is called.
     * 
     * @param id
     * @param lineNumber the line where the namespace is called for, for error reporting.
     * @return
     */
    INamespace getNamespace(String id, int lineNumber);

    /**
     * Define symbol with current namespace visibility. Won't be called on a resolver not returned by
     * getNamespaceResolver().
     * 
     * @param id
     * @param value
     * @param lineNumber
     */
    void defineSymbol(String id, Object value, int lineNumber);

    /**
     * If the passed name is unambiguous within the symbol space handled by this resolver, return it
     * unmodified; otherwise disambiguate it with the least intrusive modification possible if the second
     * parameter is true. If ambiguous and canChange is false, call onException with a validation error.
     * 
     * @param name
     * @param canChange
     * @return
     */
    String validateObjectName(String name, boolean canChange, int lineNumber);

    /**
     * Return the namespace-specific symbol table that is filled in by the parser using defineSymbol(). It
     * will not be called on a resolver not returned by getNamespaceResolver().
     * 
     * @return
     */
    Map<String, Object> getSymbolTable();

    /**
     * If the resolver is for a project, as it should be, return the project. Namespace resolvers are expected
     * to return the project that the namespace belongs to.
     * 
     * @return
     */
    IProject getProject();

    /**
     * Pass a function call, warn or complain if it doesn't match a known prototype.
     * 
     * @param ret
     * @return
     */
    boolean validateFunctionCall(IFunctionCall ret);

    /**
     * Return a language adapter for the expression language that this namespace expects.
     * 
     * @return
     */
    IExpressionLanguageAdapter getLanguageAdapter();

    /**
     * Called on a namespace resolver whenever an import statement for another namespace is found. Must return
     * a valid namespace or null - if the return value is null, the parser will generate an error.
     * 
     * The normal way this should proceed is by asking the project to find the namespace, loading if
     * necessary, in it or any of its prerequisites, which should be already loaded by the time this is
     * called.
     * 
     * @param id
     * @param lineNumber
     * @return
     */
    INamespace importNamespace(String id, int lineNumber, IMonitor monitor);

    IClassifier getUniversalClassifier();

    IClassifier getNumberMatcherClassifier(Number n);

    IClassifier getRangeMatcherClassifier(NumericInterval numericInterval);

    IClassifier getNullMatcherClassifier();

    IClassifier getMultipleClassifier(IList nodeToList);

    IClassifier getConceptMatcherClassifier(IConcept conceptFor);

    IClassifier getStringMatcherClassifier(String string);

    IClassifier getBooleanMatcherClassifier(boolean value);

    /**
     * proxy to the core concept resolver
     * @param name
     * @return
     */
    IConcept getConcept(String name);

    /**
     * proxy to the core concept resolver
     * @param name
     * @return
     */
    IProperty getProperty(String name);

    /**
     * Return a function call that will interrogate the passed lookup table with the passed arguments names.
     * Arguments are resolved to values from the function's context and correspond in number with the number
     * of columns. One and only one argument is named "?" and corresponds to the column to look into.
     * 
     * @param lookupTable
     * @param args
     * @return
     */
    IFunctionCall getTableLookupFunctionCall(ITable lookupTable, List<String> args);

    /**
     * Retrieve or create the concept that corresponds to the passed one adopting the passed traits. This
     * concept should belong to the same ontology than the primary concept and be named in an understandable
     * and unambiguous way.
     * 
     * @param concept
     * @param traits
     * @return
     */
    IConceptDefinition addTraits(IConcept concept, ArrayList<IConcept> traits, int lineNumber);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    public boolean isParticular(IConcept concept);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    public boolean isQuality(IConcept concept);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    public boolean isObject(IConcept concept);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    public boolean isProcess(IConcept concept);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    boolean isObservable(IConcept c);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    boolean isTrait(IConcept cc, boolean isBaseTrait);

    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    boolean isClass(IConcept ccl);
    
    /**
     * Proxies to the core identities in Thinklab. The reference implementation for these is in NS in the
     * common package.
     * 
     * @param concept
     * @return
     */
    boolean isSubjective(IConcept concept);
    
    /**
     * Create a classification that will classify subclasses of the passed concept according to
     * the particular base trait passed.
     *  
     * @param cc
     * @param concept
     * @return
     */
    IClassification createTraitClassification(IConcept cc, IConcept concept);

    /**
     * Return the trait space for the given trait.
     * 
     * @param traitType
     * @return
     */
    Collection<IConcept> getTraitSpace(IConcept traitType);

    /**
     * Proxy to the knowledge manager's getAuthority().
     */
    IAuthority getAuthority(String id);

    /**
     * Validate the passed observable concept in the observation and role passed. The context is any other observable necessary to
     * complete validation: if observable is in main role, no context will be passed. Call addError on the IObserver for anything
     * invalid.
     * 
     * @param concept
     * @param measurement
     * @param main
     * @param i
     */
    void validateObservable(IConceptDefinition observable, ObservableRole role, IObserver ret, int line, IConceptDefinition... context);



}
