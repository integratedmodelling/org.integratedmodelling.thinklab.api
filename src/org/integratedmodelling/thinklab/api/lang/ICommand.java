package org.integratedmodelling.thinklab.api.lang;

import org.integratedmodelling.thinklab.api.listeners.IMonitorable;

public interface ICommand extends IMonitorable {

    /**
     * Argument types for validation. 
     * 
     * LINE means that everything 
     * after matching the argument is gobbled up until the end of the
     * command - of course only feasible if there is exactly one argument.
     * 
     * BOOLEAN is only for options and it means that you don't pass a value - you just
     * pass the option or don't.
     */
    public static final int TEXT = 0;
    public static final int NUMBER = 1;
    public static final int INTEGER = 2;
    public static final int FLOAT = 3;
    public static final int DATE = 4;
    public static final int CONCEPT = 5;
    public static final int MODEL = 6;
    public static final int SUBJECT = 7;
    public static final int LINE = 8;
    public static final int PROPERTY = 9;
    public static final int BOOLEAN = 10;

    /**
     * Get the prototype this command has been matched to. If a ICommand exist, there
     * must be a prototype, so this is never null.
     * 
     * @return
     */
    IPrototype getPrototype();

    /**
     * Get the command or the option value identified by the passed ID. It will have the
     * appropriate type according to prototype.
     * 
     * @param argumentId
     * @return
     */
    Object get(String argumentId);

    /**
     * True only when the (optional) argument is passed. If it's not a BOOLEAN, true
     * for this one will correspond to a non-null value of get().
     * 
     * @param argumentId
     * @return
     */
    boolean has(String argumentId);

    /**
     * A command is a task, so it gets a task id with which it can be recognized, 
     * interrupted by a monitor, or anything else. 
     * 
     * @return
     */
    int getTaskID();
}
