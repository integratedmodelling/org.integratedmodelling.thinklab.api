package org.integratedmodelling.thinklab.api.modelling;

import java.util.Map;

public interface IClassifier {

    public boolean classify(Object o);

    boolean isUniversal();

    boolean isNil();

    boolean isInterval();

    void negate();

    /**
     * Return code that would define this classifier in a default host language. Used for
     * debugging.
     * 
     * @return
     */
    public String dumpCode();

    /**
     * Classifiers may be used as a value; this one should return the most appropriate 
     * value translation of the classifier, i.e. the matched object if it's matching a
     * single one, or possibly a random object among the choices if it's in OR.
    
     * @return
     */
    public Object asValue();
}
