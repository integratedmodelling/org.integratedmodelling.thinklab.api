package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;

public interface IObjectSource {

    /**
     * Return the list of objects in this scale. A IReifiableSubject is a data structure that allows subjects to be
     * created as required.
     * 
     * @param scale
     * @return
     */
    public abstract List<IReifiableObject> getObjects(IScale scale);

    /**
     * Returns a state accessor for the dereified quality corresponding to the passed attribute and
     * observer.
     * 
     * @param attribute
     * @param observer
     * @param context
     * @param monitor
     * @return
     * @throws ThinklabException
     */
    IStateAccessor getAccessor(String attribute, IObserver observer, IScale context, IMonitor monitor)
            throws ThinklabException;

    /**
     * Get the scale for this object source.
     * @return
     */
    IScale getCoverage();

    /**
    * Return an accessor that will observe the quality defined by the observer over the
    * totality of the subjects visible over the passed scale. This is a de-reifying
    * operation that will turn an aspect of the objects into a quality for another
    * subject. Must be passed the attribute of the objects that serves as the basis
    * for the conversion. Some attribute IDs (e.g. for presence/absence) may be
    * predefined by the infrastructure.
    *
    * @param attribute
    * @param observer
    * @param context
    * @param monitor
    * @return
    * @throws ThinklabException
    */
    public abstract IDataSource getDatasource(String attribute);

}
