package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;

/**
 * 
 * @author Ferd
 *
 */
public interface IClassification extends ILanguageDefinition {

    /**
     * @author   Ferd
     */
    public enum Type {
        UNORDERED,
        BOOLEAN_RANKING,
        ORDERED_RANKING,
        RANGE_DISCRETIZATION
    }

    /**
     * This must be known after initialization. The classification should use hints coming from
     * the concept, the observer type etc. to determine the type. There is also a type hint that
     * can be explicitly submitted in the IClassificationDefinition interface.
     * 
     * @return
     */
    public Type getType();

    /**
     * Return the root concept for the classification. All concepts that can be attributed
     * to the classifiers should be direct, disjoint children of it.
     * 
     * @return
     */
    public IConcept getConceptSpace();

    /**
     * Return the concept that the passed object classifies to.
     * 
     * @param o
     * @return
     */
    public IConcept classify(Object o);

    /**
     * Return all the classifiers that define the classification. If the classifiers have
     * an intrinsic ordering, this function should return them in an order that reflects it.
     * @return
     */
    public List<IClassifier> getClassifiers();

    /**
     * Get the concept ordering implied by the definition - which may be the order
     * of declaration, but also have been redefined based e.g. on the discretization
     * intervals, and may not correspond to the ordering of the classifiers.
     * 
     * @return
     */
    public List<IConcept> getConceptOrder();

    /**
     * If the classification discretizes a numeric range, this must return
     * the breakpoints of the range, i.e. an array of n+1 elements (n = number
     * of concepts) where each interval {r[n], r[n+1]} corresponds to the numeric range
     * of concept n. 
     * 
     * @return
     */
    public double[] getDistributionBreakpoints();

    /**
     * Return true if one of the possible rank concepts corresponds to a logical
     * zero - e.g. NoHousehold or WetlandAbsent.
     * 
     * @return
     */
    public boolean hasZeroRank();

    /**
     * Return true if the classes in this classification are supposed to mean just
     * that - disjoint classes not representing a numeric range or a logical ranking.
     * 
     * @return
     */
    public boolean isCategorical();

    /**
     * Return true if all the intervals are contiguous and the extreme intervals have
     * finite boundaries.
     * 
     * @return
     */
    public boolean isContiguousAndFinite();

    /**
     * True if this classification has been explicitly indicated as a discretization. If
     * this is true, isContiguousAndFinite() must be also true.
     * @return
     */
    public boolean isDiscretization();

    /**
     * Check with another classification and ensure they do exactly the same things. 
     * Could be "equals"- FIXME check if that creates any problem.
     * 
     * @param _classification
     * @return
     */
    public boolean isIdentical(IClassification classification);

}
