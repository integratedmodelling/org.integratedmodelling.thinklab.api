package org.integratedmodelling.thinklab.api.modelling;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * A Model, i.e. a semantic annotation. When unresolved (not linking to states), it 
 * is a query that uses observers to produce its result observation. It 
 * must have at least one observable. If it has more, they must have been given
 * observation semantics through other models in the same namespace.
 * 
 * Models may be unresolved (i.e. they leave their observable specified only at a
 * semantic level) or resolved (they specify an actual datasource and the observation
 * semantics for it). 
 * 
 */
public interface IModel extends IObservingObject {

    /**
     * Return the semantics of all observables we are observing. The first
     * in the list is the actual observable and must exist; the others are
     * expected side-effects of observing the first, which must be connected
     * to semantics (i.e. they are specified along with observers in the
     * language, or have correspondent, unambiguous models in the same namespace).
     * 
     * Secondary observables must be qualities even in agent models.
     * 
     * TODO make this return a list of concepts - these are semantic annotations.
     * 
     * @return
     */
    public List<IObservable> getObservables();

    /**
     * Return the datasource, if any. If we have a datasource, the observer 
     * defines its observable through its observed "endpoint" - either an
     * observable object or a model. The datasource is attributed to the 
     * model, not to the observer, which interprets it rather than
     * own it. Models with datasources must be quality models that
     * produce states, and their isReificationModel() method
     * returns false.
     *  
     * @return
     */
    public IDataSource getDatasource();

    /**
     * Return the object source if one was provided. Models with an object source
     * are of course subject models, and have no observers (because the observation
     * is a direct observation). 
     * 
     * @return
     */
    public abstract IObjectSource getObjectSource();

    /**
     * This will only be called in models that produce objects (isReificationModel() == true) and
     * have defined observers for attributes of the objects produced. It is used to create
     * de-reifying data models by "painting" the object attributes over the context, ignoring
     * the identity of the objects. For each attribute name returned, the method getAttributeObserver()
     * must return a valid observer. Some of the attributes may be internally generated: for example,
     * it is always possible to infer 'presence of' an object from an observation of the object 
     * itself.
     * 
     * @return
     */
    public Collection<String> getObjectAttributes();

    /**
     * Called for each return value of getObjectAttributes() to produce de-reifying models for
     * the attribute in question, which are able to use the objects as a data source for the
     * object's quality in question. If the attribute is null, produce a presence/absence 
     * model.
     * 
     * @param attribute
     * @return
     */
    public IModel getAttributeObserver(String attribute);

    /**
     * Return the observer that made this observation and provides the
     * full observation semantics for it. Data models have one observer, which
     * may be a IConditionalObserver switching to others according to context. Object
     * models (agent models) have no observer and their isReificationModel() method
     * returns true.
     * 
     * @return
     */
    public IObserver getObserver();

    /**
     * Return true if this model can be computed on its own and has associated data. Normally
     * this amounts to getDatasource() != null && getDependencies().size() == 0, but implementations
     * may provide faster ways to inquire (e.g. without creating the datasource).
     * 
     * @return
     */
    public boolean isResolved();

    /**
     * Return the accessor that was specified by the user, if any.
     * @param monitor TODO
     * 
     * @return
     */
    public abstract IAccessor getAccessor(IScale context, IMonitor monitor) throws ThinklabException;

    /**
     * Return true if there's any reason why we shouldn't use this model.
     * 
     * @return
     */
    @Override
    public abstract boolean hasErrors();

    /**
     * True if the model produces object annotations.
     * 
     * @return
     */
    public abstract boolean isReificationModel();

}
