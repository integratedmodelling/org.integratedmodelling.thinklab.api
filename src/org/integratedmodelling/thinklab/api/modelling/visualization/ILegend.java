package org.integratedmodelling.thinklab.api.modelling.visualization;

/**
 * A Legend is a specialized media that makes the associated media understandable. Not all media
 * need legends.
 * 
 * @author Ferd
 *
 */
public interface ILegend extends IMedia {

    /*
     * flags for description types
     */
    /**
     * Short description (40 characters or less)
     */
    public static int SHORT_DESCRIPTION = 0;

    /**
     * Long description (arbitrary length)
     */
    public static int VERBOSE_DESCRIPTION = 1;

    /**
     * One word
     */
    public static int LABEL = 2;

    /**
     * Same colormap returned by IMedia.getColormap(), for convenience.
     * @return
     */
    public IColormap getColormap();

    /**
     * Obtain a textual description of the passed object. The object will usually
     * be returned by IMedia.getValue(). Flags can be used to customize the description.
     * 
     * @param data
     * @param type one of the constants above
     * @return
     */
    public String getDescription(Object data, int type);

    /**
     * Obtain a textual description of the passed type for the whole object described
     * by this.
     * 
     * @param flags
     * @return
     */
    public String getDescription(int type);

    /**
     * Get the object at the given offset.
     * 
     * @param offset
     * @return
     */
    public Object getValue(int offset);
}
