package org.integratedmodelling.thinklab.api.modelling.visualization;

import javax.activation.MimeType;

/**
 * IMedia is an abstract interface for the product of any kind of visualization (which may involve other
 * media than just visual).
 * 
 * @author Ferd
 *
 */
public interface IMedia {

    /**
     * MIME type for the media.
     * @return
     */
    MimeType getMIMEType();

    /**
     * Additional info to interpret the media. May be null.
     * @return
     */
    ILegend getLegend();

    /**
     * Fit to a different viewport if possible, or return null.
     * 
     * @param viewport
     * @return
     */
    IMedia scale(IViewport viewport);

}
