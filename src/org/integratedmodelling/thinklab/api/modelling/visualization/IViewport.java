package org.integratedmodelling.thinklab.api.modelling.visualization;

/**
 * Represent a view for media to fit into - whatever that means, either an image size and DPI, an audio
 * resolution, etc. Tag interface which can be specialized as needed and whose detailed implementation
 * depends on the media requested. The common package should have a ViewportFactory that will create
 * appropriate viewports for different media.
 * 
 * @author Ferd
 *
 */
public interface IViewport {

}
