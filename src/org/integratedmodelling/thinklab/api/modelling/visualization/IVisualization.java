package org.integratedmodelling.thinklab.api.modelling.visualization;

import java.io.File;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Visualization for a subject. Produces media for the context and visualizations for
 * all states and subject linked to it.
 * 
 * TODO rediscuss
 * 
 * @author Ferd
 *
 */
public interface IVisualization {

    /**
     * Get the requested media type for the context. This will be e.g. a satellite
     * image for a spatial object, a timeline for a temporal, etc.
     * 
     * @param directory
     * @param mediaType
     * @param viewportWidth
     * @param viewportHeight
     * @param flags
     * @return
     * @throws ThinklabException
     */
    public IMedia getMedia(File directory, int mediaType, int viewportWidth, int viewportHeight, int flags)
            throws ThinklabException;

    /**
     * Get visualizations for all states. 
     * 
     * @return
     */
    public List<IStateVisualization> getStateVisualizations();

    /**
     * Get visualizations for all subjects.
     * 
     * @return
     */
    public List<IVisualization> getSubjectVisualizations();

    /**
     * Persist to given location. Return a location URI where this or another visualization 
     * can be restored from. Should be capable of taking null for a
     * location, creating its own storage in a default area.
     * 
     * @param location
     * @return
     * @throws ThinklabException
     */
    public abstract String persist(String location) throws ThinklabException;

    /**
     * Name of the subject being described, or the dataset being visualized.
     * 
     * @return
     */
    String getName();

}
