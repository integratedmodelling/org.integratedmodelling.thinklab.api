package org.integratedmodelling.thinklab.api.modelling.visualization;

import java.io.File;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A visualization is specific of a state in a context and is produced from them by the
 * visualization factory. 
 * 
 * @author Ferd
 *
 */
public interface IStateVisualization {

    /*
     * values for flags
     */
    static public final int OPAQUE_BACKGROUND = 0x0001;
    static public final int EARTH_IMAGE_BACKGROUND = 0x0002;
    static public final int USE_SMOOTHING = 0x0004;

    /**
     * Return the most appropriate media type for the state we represent. Pass flags if 
     * any special type of visualization is desired.
     * 
     * @param directory
     * @param viewportWidth
     * @param viewportHeight
     * @return
     * @throws ThinklabException
     */
    public IMedia getMedia(File directory, int viewportWidth, int viewportHeight, int flags)
            throws ThinklabException;

    /**
     * Return a media file of the passed type. Throw an exception if the type is inappropriate
     * for the context.
     * 
     * @param directory
     * @param mediaType pass one of the values in IMedia.
     * @param viewportWidth
     * @param viewportHeight
     * @return
     * @throws ThinklabException
     */
    public IMedia getMedia(File directory, int mediaType, int viewportWidth, int viewportHeight, int flags)
            throws ThinklabException;

    /**
     * Return an index (0-255 max) corresponding to the object passed in the visualization created
     * for our state.
     * 
     * @param o
     * @return
     */
    public byte getVisualizedValue(Object o);

    /**
     * Called only after getMedia has been called once. Should return the same media that getMedia returned, with
     * the new viewport. It should be used so that the largest media size is requested in getMedia and only smaller
     * ones get rescaled requests.
     * 
     * @param thumbDir
     * @param visual
     * @param thumbnailViewportWidth
     * @param thumbnailViewportHeight
     * @param _flags
     * @return
     */
    public IMedia getRescaledMedia(File thumbDir, int mediaType, int viewportWidth, int viewportHeight,
            int flags);

}
