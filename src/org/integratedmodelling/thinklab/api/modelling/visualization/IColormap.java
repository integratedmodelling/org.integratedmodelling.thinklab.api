package org.integratedmodelling.thinklab.api.modelling.visualization;

import java.awt.Color;
import java.awt.image.IndexColorModel;

public interface IColormap {

    /**
     * 
     * @return
     */
    int getColorCount();

    /**
     * 
     * @return
     */
    IndexColorModel getColorModel();

    /**
     * 
     * @return
     */
    boolean hasTransparentZero();

    /**
     * 
     * @param index
     * @return
     */
    Color getColor(int index);

}
