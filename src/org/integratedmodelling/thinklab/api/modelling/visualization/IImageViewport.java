package org.integratedmodelling.thinklab.api.modelling.visualization;

/**
 * Represent a visualization view - whatever that means, either an image region, an audio file
 * location, etc. Tag interface which can be specialized as needed and whose detailed implementation
 * depends on the media requested. The common package should have a ViewportFactory that will create
 * appropriate viewports for different media.
 * 
 * @author Ferd
 *
 */
public interface IImageViewport extends IViewport {

    int getWidth();

    int getHeight();

    int[] getSizeFor(double imageWidth, double imageHeight);

}
