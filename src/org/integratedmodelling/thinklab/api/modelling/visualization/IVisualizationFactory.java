package org.integratedmodelling.thinklab.api.modelling.visualization;

import java.util.Map;

import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.IScale;

/**
 * The visualization factory produces media for an observation according to type and 
 * (optional) preferences. Should use caching sensibly as the same media may be requested
 * multiple times.
 * 
 * @author Ferd
 *
 */
public interface IVisualizationFactory {

    /**
     * Create the appropriate media for the passed observation and the MIME type requested, or null
     * if the media cannot be created. Used for visualization, export or anything else where observations
     * must be exported to other applications.
     * 
     * @param observation
     * @param index the slice of the data we want to visualize (e.g. a temporal or spatial snapshot of a T/S
     *        context). Pass null if not applicable (e.g. on a subject) or if all data are requested (which 
     *        of course must match the requested media type).
     * @param viewport the viewport (whatever that means for the media type). May be null even in media that
     *        need one, such as images - in that case, a default should be provided.
     * @param mimeType
     * @param options parameters that may influence the result. May be null.
     * 
     * @return the media, or null if the media cannot be produced.
     */
    public IMedia getMedia(IObservation observation, IScale.Index index, IViewport viewport, String mimeType,
            Map<String, Object> options);

}
