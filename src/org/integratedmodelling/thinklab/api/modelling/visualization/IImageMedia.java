package org.integratedmodelling.thinklab.api.modelling.visualization;

import java.awt.Image;

import org.integratedmodelling.exceptions.ThinklabException;

public interface IImageMedia extends IMedia {

    /**
     *
     * @return
     */
    IColormap getColormap();

    /**
     * 
     * @return
     */
    IViewport getViewport();

    /**
     * 
     * @return
     * @throws ThinklabException 
     */
    Image getImage() throws ThinklabException;

}
