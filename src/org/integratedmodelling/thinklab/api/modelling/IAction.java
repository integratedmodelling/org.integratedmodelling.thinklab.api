package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;

/**
 * Actions may be connected to accessors to operate after their values have been computed. 
 * In state accessors, actions may change, integrate, reinterpret observable values, or compute
 * the value of the observable in computing observers. In
 * subject accessors, they may modify the state or context of the subject. Actions are 
 * triggered by transitions; a set of domain concepts identifies the specific transitions
 * that the action is sensitive to.
 * 
 * @author Ferd
 *
 */
public interface IAction {

    /*
     * Types of actions. These will see lots of development.
     */

    /**
     * Change the state of a target observable (or the main one) to the result of running the action.
     */
    public static final int CHANGE = 0;

    /**
     * Change the state of a target observable (or the main one) to the integrated result of running
     * the action describing instantaneous change in time.
     */
    public static final int INTEGRATE = 1;

    /**
     * Modify the spatial context for the target subject when run.
     */
    public static final int MOVE = 2;

    /**
     * Arbitrary operations with potential side-effects; values returned are ignored.
     */
    public static final int DO = 3;

    /**
     * Delete the target subject.
     */
    public static final int DESTROY = 4;

    /**
     * Return the array of domain concepts that select the transitions upon which 
     * this action will be run. If the return value is null or empty, the action
     * is expected to run at initialization only.
     * 
     * @return
     */
    public IConcept[] getDomain();

}
