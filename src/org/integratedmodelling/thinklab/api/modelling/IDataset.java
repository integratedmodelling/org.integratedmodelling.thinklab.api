package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A IDataset is a persistent ISubject. The objects returned should be API-legitimate but will not be expected
 * to take part in "live" contextualization.
 * 
 * Should have an overridden getSubjects() that returns a collection of IDataset, but Java does not let us do
 * that in any acceptable way.
 * 
 * @author Ferdinando Villa
 */
public interface IDataset extends ISubject {

    /**
     * Ensure we can get GC's without losing data. We should know where. Return a location URI it can be
     * restored from. Should be capable of taking null for a location, creating its own storage in a default
     * area.
     * 
     * @throws ThinklabException
     */
    public abstract String persist(String location) throws ThinklabException;

    /**
     * Read the dataset from assigned storage.
     * @throws ThinklabException
     */
    public abstract void restore(String location) throws ThinklabException;

    /**
     * Used when the dataset is used for "live" backing of states. Close the dataset, delete the backing store
     * and free any resource. No other methods should be called after this one.
     */
    public abstract void dispose();

}
