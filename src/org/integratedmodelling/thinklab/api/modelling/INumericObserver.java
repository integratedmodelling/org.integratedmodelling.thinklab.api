package org.integratedmodelling.thinklab.api.modelling;

/**
 * Numeric observers produce numeric states but may have a discretization. Numeric
 * states may be probabilistic, too.
 * 
 * @author Ferd
 */
public interface INumericObserver extends IObserver {

    /**
     * Return the discretization if any, or null.
     * 
     * @return
     */
    public abstract IClassification getDiscretization();

    /**
     * If a minimum value is defined, return it, otherwise return NaN.
     * @return
     */
    public double getMinimumValue();

    /**
     * If a maximum value is defined, return it, otherwise return NaN.
     * @return
     */
    public double getMaximumValue();

}
