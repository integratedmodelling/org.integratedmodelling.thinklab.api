package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.lang.IList;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.parsing.IFunctionCall;
import org.integratedmodelling.thinklab.api.modelling.parsing.IModelObjectDefinition;
import org.integratedmodelling.thinklab.api.modelling.parsing.IPropertyDefinition;

/**
 * Holds the declarations corresponding to an "observe" definition, which specifies an ISubject (the only
 * observation that can be stated at the statement level, because an IState cannot be specified outside of an
 * ISubject). Will generate a corresponding ISubject on demand.
 * 
 * @author Ferd
 * 
 */
public interface ISubjectGenerator extends IModelObjectDefinition {

    /**
     * Create an instance of the subject defined by this model object and return it wrapped in a subject
     * resolver that can be used to observe it.
     * 
     * Called when the subject is observed.
     * @param namespace
     * @param scenarios
     * 
     * @return
     * @throws ThinklabException
     */
    // ISubjectObserver getSubjectObserver(INamespace namespace, IMonitor monitor)
    // throws ThinklabException;

    /**
     * Create, resolve and run an instance of the subject defined by this model object. Return the fully
     * defined observation.
     * 
     * @param namespace
     * @param monitor
     * @param scenarios
     * @return
     * @throws ThinklabException
     */
    // ISubject observe(INamespace namespace, IMonitor monitor, List<String> scenarios) throws
    // ThinklabException;

    /**
     * Create a new subject, ready for initialization and contextualization.
     * 
     * @return
     * @throws ThinklabException
     */
    ISubject createSubject() throws ThinklabException;

    /**
     * Set the definition of the observable that specifies the semantic object. It may be partial - functional
     * properties will be observed automatically.
     * 
     * @param odef
     */
    void setObservable(IList odef);

    /**
     * Add a function that will be run when createSubject() is called to provide pre-observed state.
     * 
     * @param ff
     */
    void addObservationGeneratorFunction(IFunctionCall ff);

    /**
     * Force use of a specific model to choose when observing a given property, which should be functional.
     * 
     * @param property
     * @param observer
     */
    void addModelDependency(IPropertyDefinition property, IModel observer);

}
