package org.integratedmodelling.thinklab.api.modelling;

import java.util.BitSet;
import java.util.Iterator;

import org.integratedmodelling.thinklab.api.knowledge.ISemanticLiteral;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.modelling.IScale.Index;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;

/**
 * An IState is the result of observing a quality. IStates only exist in the context of an ISubject
 * and are the target of relationships defined by data properties. As such an IState classifies as
 * a literal semantic object.
 * 
 * Because ISubjects are endurant physical objects that may exist in time/space or other abstract regions
 * (an {@link IExtent} in Thinklab) whose adoption implies a distribution of observed states,
 * the IState is a complex literal that has as many states as the cartesian product of all the
 * IExtents owned by the ISubject that contains it. The demote() operation will therefore return
 * collections or atomic objects according to the extents adopted. Specialized access methods
 * return the event index of the ISchedule that represents the ISubject's view of the extents.
 * 
 * @author  Ferdinando
 */
public interface IState extends ISemanticLiteral<Object>, IMetadataHolder, IObservation {

    /**
     * Return the total number of values determined by the extents owned by
     * the owning ISubject.
     * 
     * @return
     */
    long getValueCount();

    /**
     * States are created by observers and will store them to provide a
     * link to the observation semantics.
     * 
     * FIXME this should be available now as part of the IObservable returned by getObservable().
     * 
     * @return
     */
    IObserver getObserver();

    /**
     * True if the owning ISubject has an observation of space with more than
     * one state value.
     * 
     * @return
     */
    boolean isSpatiallyDistributed();

    /**
     * True if the owning ISubject has an observation of time with more than
     * one state value.
     * 
     * @return
     */
    boolean isTemporallyDistributed();

    /**
     * True if the owning ISubject has ANY implementation of time.
     * @return
     */
    boolean isTemporal();

    /**
     * True if the owning ISubject has ANY implementation of space.
     * @return
     */
    boolean isSpatial();

    /**
     * Return the spatial extent or null.
     * 
     * @return
     */
    ISpatialExtent getSpace();

    /**
     * Return the temporal extent or null.
     * @return
     */
    ITemporalExtent getTime();

    /**
     * 
     * @param index
     * @return
     */
    Object getValue(int index);

    /**
     * Get an iterator for the states over the dimension specified in the passed index. Index is 
     * retrieved from the scale using the {@link IScale.getIndex()} function.
     * @param index
     * @return
     */
    Iterator<Object> iterator(Index index);

    /**
     * States are expected to have uniform values. This one should return the
     * most general type of the value (i.e. a Thinklab API interface or POD type), 
     * or null if there are no values.
     * 
     * @return
     */
    Class<?> getDataClass();

    /**
     * States may have a mask that deactivates (turns to no-data) values that we need to
     * maintain in the state. If so, this returns a bitset that matches in numerosity the
     * state, and specifies if the object returned by getValue(i) should be considered 
     * valid (true) or no-data (false).
     * @return
     */
    BitSet getMask();

}
