package org.integratedmodelling.thinklab.api.modelling;

/**
 * A numeric observer that builds its observation by comparing two quantities. It will need to be passed the 
 * concept for the "other" quantity, either really another or the more general case of the same quantity. Such
 * observers at the moment are ratios, percentages and proportions.
 * 
 *
 * @author Ferd
 *
 */
public interface IPercentageObserver extends IRelativeObserver {

}
