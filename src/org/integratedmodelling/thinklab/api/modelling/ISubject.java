package org.integratedmodelling.thinklab.api.modelling;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;

/**
 * ISubject is the equivalent of an individual in the Thinklab modeling platform. It
 * represents the observation of an "object", i.e. the individual that results from the observation
 * of an endurant object capable of existing autonomously.
 *
 * At the most general level, the ISubject is the "agent" of most
 * modeling frameworks, and may have behavior specified as actions to be linked to any
 * context transition.
 *
 * A ISubject is a semantic object whose literal properties point to {@link IState} (observations of
 * qualities that require a ISubject to exist) and whose
 * object properties point to other ISubjects. Being the result of observation it will
 * normally be located in time/space extents (themselves IStates), and methods are provided
 * to facilitate introspection of extents, other states, and other subjects that belong
 * to the subject.
 *
 * Initial states of ISubjects are created by an {@link ISubjectGenerator} and made conformant
 * with the knowledge base by the act of observation. With the mediation of an {@link ISubjectObserver},
 * other subjects and qualities can be observed in the context of an ISubject, producing
 * valid new observations. Because Thinklab commits to OWL's open world assumption, it is possible to
 * observe things using a ISubject as the context; the operation creates a new ISubject with
 * different semantics.
 *
 * An ISubject is the result of observation, therefore it's immutable and valid during its
 * full lifetime. It is not possible to create a partially specified ISubject - the ISubjectObserver
 * is provided as a proxy to control the observation process in a fine-grained way without
 * exposing the partially specified ISubject.
 *
 * @author Ferd
 *
 */
public interface ISubject extends IMetadataHolder, IObservation, IMonitorable {

    /**
     * Return any states linked to this subject at its current location in space/time.
     * The subject should also store the properties that link the states to it.
     *
     * Although states link to IScale, which will have a temporal component for agents which experience time,
     * the states will only have meaning in one single time period within the subject's time scale.
     *
     * @return
     */
    Collection<IState> getStates();

    /**
     * Return any subjects linked to this subjects. The subject should also store
     * the properties that link the subjects to it.
     *
     * @return
     */
    Collection<ISubject> getSubjects();

    /**
     * Get all the processes that were resolved for this subject.
     */
    Collection<IProcess> getProcesses();

    /**
     * Subjects are first-class things, so they are expected to have an ID
     * beyond what their metadata say.
     *
     * @return
     */
    String getId();

    /**
     * Observe the passed observable (possibly containing a model). If initialize() hasn't been called yet, just validate and 
     * record the subject dependency. Otherwise, start the observation immediately. Throw an
     * exception if contextualize() has been called before.
     * 
     * Returns the coverage if the observation is made, and null if not (in that case the coverage will
     * be returned by initialize()).
     * 
     * @param observable
     * @param isOptional raise warnings instead of errors if the observation cannot be made.
     * @throws ThinklabException
     */
    ICoverage observe(IObservable observable, Collection<String> scenarios, boolean isOptional)
            throws ThinklabException;

    /**
     * Resolve the subject, finding a model for it first and resolving that, then resolving any
     * dependencies added with addDependency(). Can be called repeatedly as long as contextualize()
     * hasn't been called. As resolution proceeds, the resolution strategy returned by getResolutionStrategy()
     * grows.
     * 
     * @return
     * @throws ThinklabException
     */
    ICoverage initialize(Collection<String> scenarios) throws ThinklabException;

    /**
     * Call after initialize() to complete covering the context, i.e. run any time transitions. 
     * After this has run, the subject is immutable.
     */
    void contextualize() throws ThinklabException;

    /**
     * Return the resolution strategy, empty if initialize() wasn't called.
     * 
     * @return
     */
    IResolutionStrategy getResolutionStrategy();

}
