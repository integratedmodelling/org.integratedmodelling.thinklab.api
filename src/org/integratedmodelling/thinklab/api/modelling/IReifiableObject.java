package org.integratedmodelling.thinklab.api.modelling;

/**
 * Reifiable objects are returned by {@link IObjectSource} and used in reification
 * models.
 * 
 * @author Ferd
 *
 */
public interface IReifiableObject {

    /**
     * Each reified object has its own scale, which must be compatible with the
     * "overall" scale passed.
     * 
     * @return
     */
    public IScale getScale(IScale parent);

    /**
     * Annotations may specify observed for string-specified attributes of the
     * object, which must be returned as POD data compatible with the observer
     * passed. Null is a fine return value if the attribute is unknown.
     *
     * @param attribute
     * @param observer
     * @return
     */
    public Object getAttributeValue(String attribute, IObserver observer);
}
