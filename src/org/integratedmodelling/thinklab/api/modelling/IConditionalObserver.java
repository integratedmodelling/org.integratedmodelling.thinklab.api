package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;

/**
 * A conditional observer handles a set of models that handle the 
 * state according to stated conditions. Its {@link getMediator()}
 * method must accept a null parameter and return an accessor that
 * knows what to do with them.
 *  
 * @author Ferd
 *
 */
public interface IConditionalObserver extends IObserver {

    /**
     * Return the models defined in the conditional switch, in the
     * order of definition, with the expressions that select them. 
     * Expressions may be null.
     * 
     * @return
     */
    public List<Pair<IModel, IExpression>> getModels();

    /**
     * Return a non-conditional observer that represents the semantics of the states. All observers
     * must have been mediated properly before they are accepted, so this is always
     * possible.
     * 
     * @return
     */
    public IObserver getRepresentativeObserver();
}
