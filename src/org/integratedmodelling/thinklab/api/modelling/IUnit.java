package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.thinklab.api.lang.IParseable;

public interface IUnit extends IParseable {

    /**
     * Convert the given value from the passed unit to the unit we
     * represent.
     * 
     * @param value
     * @param unit
     * @return
     */
    public abstract double convert(double value, IUnit unit);

    /**
     * 
     * @return
     */
    public abstract boolean isRate();

    /**
     * 
     * @return
     */
    public abstract IUnit getTimeExtentUnit();

    /**
     * True if this unit is a density over a spatial length.
     * 
     * @return
     */
    public abstract boolean isLengthDensity();

    /**
     *  
     * @return
     */
    public abstract IUnit getLengthExtentUnit();

    public abstract boolean isArealDensity();

    /**
     * If the unit represents an areal density, return the area term with 
     * inverted exponents - e.g. if we are something per square meter, return
     * square meters. If not an areal density, return null.
     * 
     * @return
     */
    public abstract IUnit getArealExtentUnit();

    public abstract boolean isVolumeDensity();

    public abstract IUnit getVolumeExtentUnit();

    public abstract boolean isUnitless();

    /**
     * True if this unit is a density over the kind of space represented by the passed extent,
     * which may be null (in which case, obviously, the result is false). Accounts automatically
     * for the dimensionality of the space represented.
     * 
     * @param scale
     * @return
     */
    public abstract boolean isSpatialDensity(IExtent scale);
}
