package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;

/**
 * Anything that observes anything must implement this interface, normally through another that extends it.
 * 
 * @author Ferd
 * 
 */
public abstract interface IObservingObject extends IModelObject {

    public static interface IDependency extends ILanguageDefinition {

        /**
         * The object that we need to observe to satisfy this dependency.
         * @return
         */
        public IObservable getObservable();

        /**
         * Whether we can do without this dependency or not.
         * @return
         */
        public boolean isOptional();

        /**
         * The property that expresses the counterpart of this dependency in the conceptual model for the
         * observable of the embedding model. May be null. If not, the dependency resolution strategy must
         * honor the restrictions of the property in the context of each matched observable for the embedding
         * model.
         * 
         * @return
         */
        public IProperty getProperty();

        /**
         * If true, the dependency should be for an agent that is distributed over the domain returned by
         * running the model returned by getDistributionContext() in the context of observation.
         * @return
         */
        boolean isDistributed();
        
        /**
         * If true, the dependency has the "every" qualifier, meaning we want to observe not one
         * concept, but all the disjoint concrete children of it as separate observations.
         * 
         * @return
         */
        boolean isGeneric();

        /**
         * Return whatever object was passed to contextualize the dependency (the 'at' part of the
         * specification). It can be anything that generates context - either extents, models, or a function
         * call that creates that.
         * 
         * @return
         */
        Object getContextModel();

        /**
         * The object, if any, which
         * @return
         */
        Object getWhereCondition();

        /*
         * 
         */
        String getFormalName();

    }

    /**
     * Dependency information using the structure above.
     * 
     * @return
     */
    public List<IDependency> getDependencies();

    /**
     * Given the observable for a dependency and that of an extent implemented, return whether the model has
     * any actions that define the state of that dependency at domain transitions for that domain concept.
     * This will tell the API whether the dependency needs to be supplied in full or we only need to supply
     * enough state to initialize it.
     * 
     * @param observable
     * @param domainConcept
     * @return
     */
    boolean hasActionsFor(IConcept observable, IConcept domainConcept);

    /**
     * If a model was given a specific coverage in any extent, either directly through its actions or through
     * a namespace-wide specification, return the scale that expresses that coverage. If no coverage has been
     * specified, return an empty scale.
     * 
     * @return
     * @throws ThinklabException
     */
    public abstract IScale getCoverage() throws ThinklabException;

    /**
     * Return any action given in the specifications. If none was given, return an empty list.
     * 
     * @return
     */
    public List<IAction> getActions();

    /**
     * This one is fundamental for proper operation: if it returns false, the observer is expected to be
     * linked to a source of states for its concept (data or other model). If it returns true, the observer is
     * treated as capable of computing its data based only on its dependencies, and no resolution of the
     * observable is attempted.
     * 
     * @return
     */
    public boolean isComputed();

    /**
     * Return the observable (i.e. the dolce:quality) being observed. In IModel, this returns 
     * the first observable even if the model has more than one.

     * @return
     */
    public IObservable getObservable();

    /**
     * If the observer is looking at a quality of a specific subject type, this returns the type of the
     * subject.
     * 
     * @return
     */
    public IConcept getInherentSubjectType();

    /**
     * generate and return a scale object by analyzing the scale functions identified in the model. this is
     * mainly for IModel, but may have uses elsewhere. It's defined here because the scale generation
     * algorithm uses state stored in ObservingObject\<T\>.
     * @return
     */
    public IScale getScale();
}
