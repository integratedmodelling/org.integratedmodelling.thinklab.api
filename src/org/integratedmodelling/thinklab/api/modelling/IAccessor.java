package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

/**
 * Accessors give access to the raw values of observations and are used to process their data or objects. A model
 * may have zero or more accessors; all state models have an accessor, not all subject models need to. When an accessor
 * is specified in the language, it is chained to the "natural" accessor for state models, and it's used as the 
 * natural accessor in subject models, which do not have one by default.
 * 
 * IStateAccessors operate serially in a context, being passed each state in the sequence determined by the context's internal 
 * topologies. State models always have a default accessor and may be chained to a user-provided one for further processing.
 * 
 * ISubjectAccessors can be attached to subject models and when present, receive the fully computed
 * state of the subject over the whole context. They do not exist by default, but user accessors may be
 * defined to provide specific computations. Any complex model being wrapped semantically is essentially
 * a ISubjectAccessor as an entry point. 
 * 
 * @author Ferdinando
 *
 */
public abstract interface IAccessor {

    /**
     * All accessors must have a name. Their name may be translated to a formal name in
     * a dependency or mediation link.
     * 
     * @return
     */
    public String getName();

    /**
     * Accessors may have a sequence of IActions connected to them. If so, they are
     * triggered if the current computation context is the result of the transitions
     * the action responds to.
     * 
     * @return
     */
    public List<IAction> getActions();
}
