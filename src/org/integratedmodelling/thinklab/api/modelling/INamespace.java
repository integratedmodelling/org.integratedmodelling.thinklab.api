package org.integratedmodelling.thinklab.api.modelling;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IOntology;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.project.IProject;

/**
 * @author  Ferd
 */
public interface INamespace extends IMetadataHolder {

    /**
     * The name of the namespace - full path if dot-separated.
     * @return
     */
    String getId();

    /**
     * Time of creation of the underlying resource if any, time when the object definition 
     * finished otherwise.
     * 
     * @return
     */
    long getTimeStamp();

    /**
     * A namespace is the endorsed specification of any ontology in Thinklab (IOntology is only
     * for bridging to the underlying knowledge infrastructure). Because individuals are dealt with
     * at the Thinklab/Java level using ISemanticObject, only concepts and properties are of interest.
     * 
     * @param s the requested concept ID.
     * @return a concept or null if not defined.
     */
    IConcept getConcept(String s);

    /**
     * A namespace is the endorsed specification of any ontology in Thinklab (IOntology is only
     * for bridging to the underlying knowledge infrastructure). Because individuals are dealt with
     * at the Thinklab/Java level using ISemanticObject, only concepts and properties are of interest.
     * 
     * @param s the requested property ID.
     * @return a property or null if not defined.
     */
    IProperty getProperty(String s);

    /**
     * Return all objects that were explicitly created in the language statements that 
     * produced this namespace. Implicit concepts/properties and imported objects will not be
     * returned. 
     *  
     * @return
     */
    List<IModelObject> getModelObjects();

    /**
     * Get a model object by name. Will not return implicit concepts/properties and anonymous
     * objects.
     * 
     * @param mod
     * @return
     */
    IModelObject getModelObject(String mod);

    /**
     * Return the project that this namespace was created from. Currently it can be null but
     * shouldn't - external import units should be projects and interactive sessions should 
     * operate within a specialized project.
     * 
     * @return
     */
    IProject getProject();

    /**
     * Return all namespaces imported by this one.
     * 
     * @return
     */
    Collection<INamespace> getImportedNamespaces();

    /**
     * String representation of the URL this namespace was read from. Should not be null
     * unless this is an interactive namespace.
     * 
     * @return
     */
    String getResourceUrl();

    /**
     * @return
     */
    List<String> getTrainingNamespaces();

    /**
     * @return
     */
    List<String> getLookupNamespaces();

    /**
     * Return the plug-in language that is used in this namespace to parse expressions.
     * @return
     */
    String getExpressionLanguage();

    /**
     * If a model was given a specific coverage in any extent, either directly or through
     * a namespace-wide specification, return the context that
     * expresses that coverage. If no coverage has been specified, return an empty
     * context.
     * 
     * @return
     */
    IScale getCoverage();

    /**
     * Return true if the namespace has any errors that will prevent the use of its
     * model objects.
     * 
     * @return
     */
    boolean hasErrors();

    /**
     * Return true if the namespace has warnings that should be reported before use.
     * 
     * @return
     */
    boolean hasWarnings();

    /**
     * Get all errors in the namespace: error code, message and line number (1-based, 0 for no line)
     * @return
     */
    Collection<Triple<Integer, String, Integer>> getErrors();

    /**
     * Get all warnings in the namespace: message and line number (1-based, 0 for no line)
     * 
     * @return
     */
    Collection<Pair<String, Integer>> getWarnings();

    /**
     * Returns the ontology associated with the namespace. Not that you should do anything
     * with it.
     * 
     * @return
     */
    IOntology getOntology();

    /**
     * The namespace's symbol table should contain any model objects and define's encountered, plus
     * all the imported symbols from any imported namespaces.
     * 
     * @return
     */
    Map<String, Object> getSymbolTable();

    /**
     * Return true if this namespace is a scenario, meaning that its models will only be
     * used (and if so, preferentially) to resolve dependencies when the scenario is
     * made active.
     * 
     * @return
     */
    boolean isScenario();

    /**
     * Return the model resolution criteria defined for the namespace, or the default ones if none were
     * given. These are basically a set of weights that will apply to each model retrieved to resolve
     * a concept in order to rank them and choose the best. Resolution criteria may vary by namespace.
     * 
     * @return
     */
    IMetadata getResolutionCriteria();

    /**
     * Return all the namespaces that this should not be mixed with during
     * resolution or scenario setting.
     * 
     * @return
     */
    Collection<String> getDisjointNamespaces();

    /**
     * Return the local file this namespace was read from, or null if it wasn't read from
     * a file. Returning null means in all likelihood that this namespace will never be
     * reloaded.
     * 
     * @return
     */
    File getLocalFile();

    /**
     * If the namespace is private, each model in it is private even if not 
     * tagged as such. 
     * 
     * @return
     */
    boolean isPrivate();

    /**
     * If the namespace is inactive, each model in it is inactive even if not 
     * tagged as such. 
     * 
     * @return
     */
    boolean isInactive();
    
}
