package org.integratedmodelling.thinklab.api.modelling.resolution;

import java.util.List;

import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy.Step;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

/**
 * A resolution strategy is available for a {@link ISubject} after calling the correspondent
 * {@link ISubjectObserver.resolve()}. The strategy is composed of a series of steps corresponding
 * to the dependency groups identified for the subject. Each step has a IProvenance and a IWorkflow
 * associated, which are usually only non-trivial for data resolution strategies.
 *
 * The resolution strategy is returned by {@link ISubjectObserver.getResolutionStrategy()}.
 *
 * @author Ferd
 *
 */
public interface IResolutionStrategy extends Iterable<Step> {

    /**
     * Steps in a resolution strategy. Each step 
     * @author Ferd
     *
     */
    public interface Step {

        /**
         * If the step resolves a process or subject, return the list of models that provide
         * relevant accessors and object sources. May be null.
         * 
         * @return
         */
        public IModel getModel();

        /**
         * The provenance graph for the passed step, detailing all choices made by Thinklab to observe each
         * concept. May be null.
         *
         * @param step
         * @return
         */
        IProvenance getProvenance(int step);

        /**
         * The workflow for the passed step, detailing all the processing steps computed to
         * create the initial observation. A workflow is compiled from a provenance graph.
         * May be null, and may be not null even for process or subject steps, in which case
         * the workflow should be run first.
         *
         * @param step
         * @return
         */
        IWorkflow getWorkflow(int step);

        /**
         * Get the total coverage for this step.
         * 
         * @return
         */
        ICoverage getCoverage();
    }

    /**
     * Number of steps in this strategy.
     *
     * @return
     */
    int getStepCount();

    /**
     * Get the n-th step.
     * @param i
     * @return
     */
    Step getStep(int i);

    /**
     * Execute all steps for the passed transition. The null transition is 
     * initialization.
     * 
     * @param transition
     * @return
     */
    boolean execute(ITransition transition);

}
