package org.integratedmodelling.thinklab.api.modelling.resolution;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.kbox.IKbox;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * The special kbox that represents a (local or remote) source of observations that can be queried. If remote,
 * it's expected that the abstract knowledge has been synchronized, and that the concepts mean exactly the
 * same things at both ends.
 * 
 * @author Ferd
 * 
 */
public interface IObservationKbox extends IKbox {

    /**
     * Produce a (lazy) list of models, most likely containing proxies for models that are converted into
     * actual models at get(). Ensure that the final list iterates the models in order of decreasing appropriateness
     * for the context passed.
     * 
     * @param observable
     * @param context
     * @param prioritizer
     * @return
     * @throws ThinklabException 
     */
    List<IModel> query(IObservable observable, IResolutionContext context) throws ThinklabException;

}
