package org.integratedmodelling.thinklab.api.modelling.resolution;

import java.util.Collection;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.ISubject;

/**
 * A  SubjectResolver can resolve a context to the model strategy that observe the target set in the 
 * context. The resolve() operation returns the total extent coverage that was resolved. 
 * 
 * @author Ferd
 * @author Luke
 * 
 */
public interface ISubjectResolver {

    /**
     * Entry point; will create a root context for the subject under these scenarios and call resolve(context) on all the
     * resulting contexts.
     * 
     * @param subject
     * @param scenarios
     * @param monitor
     * @return
     * @throws ThinklabException
     */
    ICoverage resolve(ISubject subject, Collection<String> scenarios, IMonitor monitor)
            throws ThinklabException;

    /**
     * Resolve the passed context. Successful resolution generates a IProvenance graph and returns a coverage 
     * whose isEmpty() method returns false. Unsuccessful resolution generates a non-null empty coverage. The
     * context knows what it's for (it always starts with a root subject), and can be loaded with scenarios, 
     * resolution criteria and anything concerning how resolution should proceed.
     * 
     * FIXME this doesn't really need to be in the API, but we can leave it for now.
     * 
     * @param context
     * @return coverage
     * @throws ThinklabException in case of errors (unsuccessful resolution is not an error).
     */
    // ICoverage resolve(IResolutionContext context) throws ThinklabException;

}
