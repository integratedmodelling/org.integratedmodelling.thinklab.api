package org.integratedmodelling.thinklab.api.modelling.resolution;

/**
 * The provenance graph that describes the model strategy resulting from resolution. Built within
 * a {@link IResolutionContext} by a {@link ISubjectResolver}.  
 * 
 * Just a tag interface for now, but will be fleshed out with methods conformant to the Open
 * Provenance standard. Should give also access to alternative resolution paths not taken if 
 * the user desires.
 * 
 * @author Ferd
 *
 */
public interface IProvenance {

    /**
     * true if there's nothing to see.
     * 
     * @return
     */
    boolean isEmpty();

}
