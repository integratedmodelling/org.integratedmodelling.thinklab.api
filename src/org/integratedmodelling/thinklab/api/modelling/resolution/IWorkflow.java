package org.integratedmodelling.thinklab.api.modelling.resolution;

import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

/**
 * The result of compiling a IProvenance into a sequence of processing steps. Running the workflow
 * will compute initialized states for the subject it refers to. A IProvenance and the corresponding
 * IWorkflow constitute a step in a IResolutionStrategy.
 * 
 * Just a tag interface for now. Should be fleshed out when possible. Ideally a IWorkflow could
 * bridge to any scientific workflow system or other virtual machine.
 * 
 * @author Ferd
 *
 */
public interface IWorkflow  {
    
    boolean run(ITransition transition);

}
