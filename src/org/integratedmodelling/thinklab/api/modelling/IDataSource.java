package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;

/**
 * IDatasource is an object that represents states for observers to interpret. Datasources
 * incarnate the objects of semantic annotation - datasets - which are matched to Scales
 * and if the match succeeds, are capable of producing an IAccessor that will give access
 * to the states under the viewpoint of that Scale.
 *
 * A IDataSource is nothing in Thinklab without a matching IObserver that will provide
 * it with observation semantics; observers are specified within models "of" the datasource.
 * 
 * @author  Ferd
 */
public interface IDataSource extends IMetadataHolder {

    /**
     * Produce an accessor that extracts the data for a given context.If we don't understand 
     * the extents in the context, throw an exception.
     * 
     * This one must store enough context information to allow the accessor to respond properly to
     * getValue(n). Whatever happened should be recorded in the provenance records.
     * 
     * @param context
     * @param contextObserver the observer that's supposed to interpret us. Just check its type so
     * 	      that the accessor can return appropriate values to feed it as input.
     * @param monitor TODO
     * @return
     * @throws ThinklabException if the transformation cannot be handled.
     */
    public abstract IAccessor getAccessor(IScale context, IObserver contextObserver, IMonitor monitor)
            throws ThinklabException;

    /**
     * Extents covered by the data. If the datasource is constant or has no extents, return 
     * an empty scale (not null).
     * 
     * @return
     */
    public IScale getCoverage();

}
