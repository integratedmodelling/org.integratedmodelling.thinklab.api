package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.IAnnotation;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;

public abstract interface IModelObjectDefinition extends ILanguageDefinition, IModelObject {

    public void setId(String id);

    public void setNamespace(INamespaceDefinition namespace);

    public void setMetadata(IMetadataDefinition metadata);

    /**
     * private knowledge is only known within its namespace.
     * @param b
     */
    void setPrivate(boolean b);

    /*
     * an inactive object is parsed but not used for anything.
     */
    void setInactive(boolean isInactive);

    /**
     * Add an error for later reporting. Implementations may decide to throw the passed exception. Return the
     * same error passed, so we can chain it efficiently.
     * 
     * @param error
     * @param lineNumber
     */
    Throwable addError(Throwable error, int lineNumber);

    /**
     * Initialize the object as soon as its definition is complete. Called by the model parser.
     * 
     * @param resolver
     */
    public void initialize(IModelResolver resolver);

    /**
     * Add annotations. May be called 0+ times.
     * 
     * @param processAnnotation
     */
    public void addAnnotation(IAnnotation processAnnotation);

}
