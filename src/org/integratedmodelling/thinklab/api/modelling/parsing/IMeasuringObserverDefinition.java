package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IMeasuringObserver;

public interface IMeasuringObserverDefinition extends INumericObserverDefinition, IMeasuringObserver {

    public void setUnit(IUnitDefinition unit);

}
