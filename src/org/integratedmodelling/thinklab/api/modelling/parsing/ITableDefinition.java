package org.integratedmodelling.thinklab.api.modelling.parsing;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.data.ITable;

public interface ITableDefinition extends ITable {

    void define(List<String> headers, List<Object> values) throws ThinklabValidationException;

}
