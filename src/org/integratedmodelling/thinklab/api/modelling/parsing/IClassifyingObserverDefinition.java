package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.IClassifyingObserver;

public abstract interface IClassifyingObserverDefinition extends IObserverDefinition, IClassifyingObserver {

    public abstract void setClassification(IClassification classification);

    /**
     * If this is called, the concept hierarchy of the main concept should be processed and
     * all concepts in it that have the passed property in the metadata should generate the
     * correspondent classifier based on its value.
     * 
     * @param metadataProperty
     */
    public abstract void setMetadataEncodingProperty(String metadataProperty);

    /**
     * Passed true if this observer has been defined with 'discretize', imposing
     * additional constraints on the classification it embeds.
     * 
     * @param b
     */
    public abstract void setIsDiscretization(boolean b);

}
