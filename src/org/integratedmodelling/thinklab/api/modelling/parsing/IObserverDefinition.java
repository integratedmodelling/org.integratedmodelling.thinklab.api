package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.IObserver;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

public abstract interface IObserverDefinition extends IObservingObjectDefinition, IObserver {

    public void addObservable(IModelDefinition mediated);

    public void addObservable(IConceptDefinition observable);

    /**
     * For a number of reasons we pass the model this observer is part of.
     * 
     * @param model
     */
    public void setModel(IModelDefinition model);

    /**
     * Notify the model observable. This may or may not be the same as the observer's according to whether the
     * model names an observable concept (unresolved models) or not. If not the same, it will have been made a
     * subclass of it at initialization (which happens before this is called). Things like classifications may
     * want to use this one to properly handle inheritance of any internal concepts.
     * 
     * @param observableConcept
     */
    public void notifyModelObservable(IObservable observable, IModelResolver resolver);

    /**
     * Classifications and discretization can be done according to an inherited trait inherited by
     * their main type, as well as the subtypes of the type itself. If so, pass the trait concept
     * here.
     * 
     * @param trait
     */
    public abstract void setTraitType(IConceptDefinition trait);
}
