package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IProbabilityObserver;

public interface IProbabilityObserverDefinition extends IProbabilityObserver, INumericObserverDefinition {

    public void setIndirect(boolean b);
}
