package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.IClassifier;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * Augments IClassification with setting methods for parsers and languages to use.
 * 
 * @author Ferd
 *
 */
public interface IClassificationDefinition extends ILanguageDefinition, IClassification {

    /**
     * Set the main concept space. It will be called AFTER all the classifiers are in.
     * 
     * @param concept
     */
    public void setConceptSpace(IObservable concept, IModelResolver resolver);

    /**
     * Add a classifier. Called in order of definition.
     * 
     * @param concept
     * @param classifier
     * @throws ThinklabException 
     */
    public void addClassifier(IConcept concept, IClassifier classifier) throws ThinklabException;

    /**
     * This is a user definition, which needs to be validated on the classifiers.
     * 
     * @param isDiscretization
     */
    public void setDiscretization(boolean isDiscretization);

    /**
     * Called as last thing in the definition process, when all other definitions 
     * and notifications have happened.
     */
    public void initialize();

    /**
     * If this is called with true, the classification is used to mediate a "select" observer,
     * so it should prepare to deal with binary classifiers.
     * 
     * @param b
     */
    public void setBooleanRanking(boolean b);

    /**
     * If this classification is expected to classify according to a trait, set the
     * generic trait type here. This will modify the classifiers to return a trait-enriched
     * root type and will validate the stated traits against the type passed.
     * 
     * @param traitType
     */
    public void setTraitType(IConcept traitType);

}
