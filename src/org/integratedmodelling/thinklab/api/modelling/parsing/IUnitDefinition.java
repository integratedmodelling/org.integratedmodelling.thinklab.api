package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IUnit;

public interface IUnitDefinition extends IUnit, ILanguageDefinition {

    public void setExpression(String expression);

    /**
     * Return a parseable string representation of the unit. Could simply be
     * toString() but this enforces it.
     * 
     * @return
     */
    public abstract String getStringExpression();

}
