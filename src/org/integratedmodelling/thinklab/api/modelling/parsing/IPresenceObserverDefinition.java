package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IPresenceObserver;

public interface IPresenceObserverDefinition extends IObserverDefinition, IPresenceObserver {

    void setIndirect(boolean b);

}
