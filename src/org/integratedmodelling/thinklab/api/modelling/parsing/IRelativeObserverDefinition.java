package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IRelativeObserver;

public interface IRelativeObserverDefinition extends IRelativeObserver, INumericObserverDefinition {

    /**
     * Set the semantics for what we compare against. If this is not passed, we
     * are building from a pre-observed comparison (which should be a rare, if
     * at all, occurrence, but for now let's leave it as a possibility). 
     * 
     * @param concept
     */
    void setComparisonConcept(IConceptDefinition concept);

    /**
     * pass true when the comparison concept is explicit (which should be the normal situation).
     * @param b
     */
    void setIndirect(boolean b);

}
