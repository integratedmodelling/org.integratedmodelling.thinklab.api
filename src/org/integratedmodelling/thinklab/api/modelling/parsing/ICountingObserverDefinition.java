package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.ICountingObserver;

public interface ICountingObserverDefinition extends ICountingObserver, IMeasuringObserverDefinition {

}
