package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IRankingObserver;

public interface IRankingObserverDefinition extends INumericObserverDefinition, IRankingObserver {

    void setScale(Number from, Number to);

}
