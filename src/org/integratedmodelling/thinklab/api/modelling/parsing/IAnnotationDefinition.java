package org.integratedmodelling.thinklab.api.modelling.parsing;

import java.util.Map;

import org.integratedmodelling.thinklab.api.modelling.IAnnotation;

public interface IAnnotationDefinition extends ILanguageDefinition, IAnnotation {

    public void setId(String id);

    public void setParameters(Map<String, Object> parameters);

    public void setNamespace(INamespaceDefinition namespace);

}
