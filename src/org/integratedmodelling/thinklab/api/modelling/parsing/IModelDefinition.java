package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.IDataSource;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;

public interface IModelDefinition extends IObservingObjectDefinition, IModel {

    /**
     * Set the observer for the model. If {@link addConditional} is used, this one
     * should not be called.
     * 
     * @param observer
     */
    public void setObserver(IObserverDefinition observer);

    /**
     * This one adds a model to work in a switcher statement. Expression may be null.
     * If this one is used, {@link setObserver} should not be used as the observer will be 
     * reset to a conditional one with the models linked to it.
     * 
     * @param observer
     * @param expression
     */
    public void addConditional(IModelDefinition observer, IExpression expression);

    /**
     * Set the datasource.
     * 
     * @param datasource
     */
    public void setDataSource(IDataSource datasource);

    /**
     * 
     * @param observationConcept
     * @param formalName
     */
    public void addObservable(IConceptDefinition observationConcept, String formalName);

    /**
     * 
     * @param observationModel
     * @param formalName
     */
    public void addObservable(IModel observationModel, String formalName);

    /**
     * 
     * @param function
     * @param formalName
     */
    public void addObservable(IFunctionCall function, String formalName);

    /**
     * 
     * @param observable
     * @param formalName
     */
    public void addInlineObservable(Object observable, String formalName);

    /**
     * Called on models that need an ID but have not been given one and there is
     * no obvious way of figuring one out. The final ID will go through the resolver
     * to be disambiguated, so it may be different from the return value.
     * @return 
     */
    public String createId();

    /**
     * When this one is called, the model must prepare itself for interpreting objects coming from
     * the object generator (function, concept or model) as annotated agents of the passed concept.
     * Translation parameters for 'grey' object attributes will be passed later using
     * {@link addAttributeTranslator}.
     * 
     * @param concept
     * @param objectGenerator
     */
    public void setObjectReificationData(IConceptDefinition concept, Object objectGenerator);

    /**
     * Add a translation rule for an attribute of the incoming concept. The passed property may
     * be null.
     * 
     * @param attributeId
     * @param observer
     * @param property 
     */
    public void addAttributeTranslator(String attributeId, IModel observer, IPropertyDefinition property);

    /**
     * Called when the model is meant to simply wrap this observer under the given concept.
     * 
     * @param observableConcept
     */
    public void wrap(IObserverDefinition observer);

    /*
     * an inactive namespace is parsed but not used.
     */
    void setInactive(boolean isInactive);

    /**
     * Called by parser to notify that this model is within a dependency. 
     * 
     * @param b
     */
    public void setDependencyModel(boolean b);

    /**
     * Called by parser when model is interpreting the observer and doesn't add a new concept.
     * @param b
     */
    void setInterpreter(boolean b);

}
