package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IClassification;
import org.integratedmodelling.thinklab.api.modelling.INumericObserver;

public interface INumericObserverDefinition extends IObserverDefinition, INumericObserver {

    public void setDiscretization(IClassification classification);

}
