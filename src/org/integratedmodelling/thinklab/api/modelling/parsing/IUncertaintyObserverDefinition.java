package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IUncertaintyObserver;

public interface IUncertaintyObserverDefinition extends INumericObserverDefinition, IUncertaintyObserver {

    void setIndirect(boolean b);

}
