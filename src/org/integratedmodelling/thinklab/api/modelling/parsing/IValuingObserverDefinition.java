package org.integratedmodelling.thinklab.api.modelling.parsing;

import org.integratedmodelling.thinklab.api.modelling.IValuingObserver;

public abstract interface IValuingObserverDefinition extends IValuingObserver, INumericObserverDefinition {

    void setCurrency(Object o);
}
