package org.integratedmodelling.thinklab.api.modelling.parsing;

import java.io.File;

import org.integratedmodelling.thinklab.api.knowledge.IAxiom;
import org.integratedmodelling.thinklab.api.knowledge.IOntology;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.metadata.IMetadata;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.project.IProject;

public interface INamespaceDefinition extends ILanguageDefinition, INamespace {

    /**
     * Set the namespace ID. Only for resolvers and such.
     * 
     * @param id
     */
     void setId(String id);

    /**
     * Add an axiom for the namespace's tbox.
     * 
     * @param axiom
     */
     void addAxiom(IAxiom axiom);

    /**
     * Set the resource name that this namespace was created from.
     * 
     * @param resourceUrl
     */
     void setResourceUrl(String resourceUrl);

    /**
     * Timestamp of last modification - tied to the resource or creation date if
     * ephemeral.
     * 
     * @param timestamp
     */
     void setTimeStamp(long timestamp);

    /**
     * Notification of imported namespace.
     * 
     * @param namespace
     */
     void addImportedNamespace(String namespace);

    /**
     * Notification of model object read within this namespace.
     * 
     * @param modelObject
     */
     void addModelObject(IModelObjectDefinition modelObject);

    /**
     * Set project we belong to. 
     * 
     * @param project
     */
     void setProject(IProject project);

    /**
     * Set from namespace header specs if a language was defined. The language used for 
     * expressions is a namespace-scoped definition, i.e., no mixing of expression 
     * languages within the same namespace.
     * 
     * 
     * @param language
     */
     void setExpressionLanguage(String language);

    /**
     * Add functions returning an extent outside of which the model should not be applied. If this
     * is done more than once for an extent of the same type, the extents should be
     * merged. Exceptions may result from the merging.
     * 
     * @param o
     */
     void addCoveredExtent(IFunctionCall o, IModelResolver resolver, int lineNumber);

    /**
     * Add an error to the namespace. Used by the parser and resolver. Line number should be
     * 1-based, and 0 if the error doesn't come from a specific statement. Error code is
     * implementation-dependent and if not used, anything may be passed.
     * 
     * @param errorCode
     * @param errorMessage
     * @param lineNumber
     */
     void addError(int errorCode, String errorMessage, int lineNumber);

    /**
     * Add a warning to the namespace. Line number as in {@link addError()}.
     * @param warning
     * @param lineNumber
     */
     void addWarning(String warning, int lineNumber);

    /**
     * Add a namespace ID that will be used to resolve dependencies. Default when none
     * is added is to use all visible namespaces.
     * 
     * @param tns
     */
     void addLookupNamespace(String tns);
    
    /**
     * Define the resolution criteria for the namespace.
     * 
     * @param metadata
     */
     void setResolutionCriteria(IMetadata metadata);

    /**
     * Add a namespace ID that will be used to find evidence for training. Default is
     * to use all visible namespaces, which is bad. TODO may want to make it mandatory
     * for training to happen.
     *
     * @param tns
     */
    public void addTrainingNamespace(String tns);

    /**
     * Set the ontology manually. Done when the ontology is created externally to the
     * namespace.
     * 
     * @param iOntology
     */
     void setOntology(IOntology iOntology);

    /**
     * Set whether this namespace is a scenario or not.
     * 
     * @param isScenario
     */
     void setScenario(boolean isScenario);

    /**
     * Add the named namespace to the list of those that should not be
     * used for resolution when this is active.
     * 
     * @param dn
     */
     void addDisjointNamespace(String dn);

    /**
     * flush all the axioms accumulated so far and update the ontology.
     */
     void synchronizeKnowledge(IModelResolver resolver);

    boolean isInternal();

    void setInternal(boolean b);

    void setLocalFile(File file);

    /*
     * a private namespace is one where each model in it is private.
     */
    void setPrivate(boolean isPrivate);

    /*
     * an inactive namespace is parsed but not used.
     */
    void setInactive(boolean isInactive);

    /**
     * Namespace may have metadata.
     * 
     * @param processMetadata
     */
     void setMetadata(IMetadataDefinition processMetadata);

     void setDescription(String docstring);
}
