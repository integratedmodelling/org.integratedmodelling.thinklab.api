package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;

/**
 * A numeric observer that builds its observation by comparing two quantities. It will need to be passed the 
 * concept for the "other" quantity, either really another or the more general case of the same quantity. Such
 * observers at the moment are ratios, percentages and proportions.
 * 
 *
 * @author Ferd
 *
 */
public interface IRelativeObserver extends INumericObserver {

    /**
     * Return the semantics for the observable we compare against. 
     * 
     * @return
     */
    IConcept getComparisonConcept();

    /**
     * If true, the comparison observable is explicit (the normal situation). Otherwise, getComparisonConcept()
     * will return null.
     * 
     * @return
     */
    boolean isIndirect();

}
