package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.listeners.IMonitorable;

public interface IProcess extends IMetadataHolder, IObservation, IMonitorable {

}
