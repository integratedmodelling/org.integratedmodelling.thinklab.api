package org.integratedmodelling.thinklab.api.modelling;

/**
 * Tag interface that applies to a IState to mean it's constant valued, so there
 * is no need for it to produce distributed datasets or distributed media when visualized.
 * 
 * @author Ferd
 *
 */
public interface IConstantState extends IState {

}
