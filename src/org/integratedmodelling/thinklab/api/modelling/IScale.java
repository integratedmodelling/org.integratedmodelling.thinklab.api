package org.integratedmodelling.thinklab.api.modelling;

import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.lang.LogicalConnector;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.space.ISpatialExtent;
import org.integratedmodelling.thinklab.api.time.ITemporalExtent;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;

/**
 * A Scale is an immutable list of the Extents seen by a Subject, which can be compared with others
 * topologically. When the extents include time, Scales can be discretized into Transitions, which in the eye
 * of a Subject may trigger Events. Those are merged in ISchedules for contextualization.
 *
 * The IExtents must be automatically internally sorted in an appropriate and stable order for
 * contextualization.
 *
 * A IScale implementation must be properly hashable and comparable in order to be added/removed to/from
 * ISchedule and used to understand the transitions caused by IEvents.
 *
 * @author Ferd
 */
public interface IScale extends Iterable<IExtent>, ITopology<IScale> {

    /**
     * Returned by getIndex, it can return the offset indices along only one
     * of the scale extents.
     * 
     * @author Ferd
     *
     */
    public interface Index extends List<Integer> {

        /**
         * true if the index is browsing through a spatial extent.
         * @return
         */
        boolean isSpatial();

        /**
         * true if the index is browsing through a temporal extent.
         * @return
         */
        boolean isTemporal();

        /**
         * Return the domain concept of the extent we're indexing.
         * @return
         */
        IConcept getDomainConcept();

        /**
         * Get the fixed offsets of all the extents. The one we're browsing will be < 0.
         * @return
         */
        int[] getOffsets();
    }

    /**
     * We deal with space and time in all natural systems, so we expose these to ease API use.
     *
     * @return
     */
    ISpatialExtent getSpace();

    /**
     * We deal with space and time in all natural systems, so we expose these to ease API use.
     *
     * @return
     */
    ITemporalExtent getTime();

    /**
     * Total number of extents available in this Scale. Note that there may be more extents than just space
     * and/or time. Read the non-existing documentation.
     *
     * @return
     */
    int getExtentCount();

    /**
     * Merge all common extents from the given scale, using the force parameter to define how the extents are
     * merged (see IExtent.merge). Extents in common are merged according to the passed operator to compute
     * the merged extent. The adopt parameter controls whether extents in the passed scale that are not in the
     * original one appear in the result. All extents in the original scale will appear in the result.
     *
     * Must not modify the original scales.
     *
     * @param scale
     * @param force
     * @return
     * @throws ThinklabException
     */
    IScale merge(IScale scale, LogicalConnector how, boolean adopt) throws ThinklabException;

    /**
     * Return true only if he scale has > 0 extents and any of them is empty, so that the coverage of any
     * other scale can only be 0.
     *
     * @return
     */
    boolean isEmpty();

    /**
     * Get an index to loop over one dimension (set as -1) given fixed position for all others.
     * 
     * @param dimensions
     * @return
     */
    Index getIndex(int... dimensions);

    /**
     * Take an array of objects that can locate a position in each extent, using the order of the extents
     * in the scale, and return the overall offset of the correspondent state (or -1 if not compatible). The
     * objects will be coordinates in the native reference system of each extent, and should be interpreted
     * correctly; each extent may take one or more objects from the list according to which object is passed
     * (e.g. a spatial point vs. lat and lon doubles).
     * 
     * @param locators
     * @return
     */
    long locate(Object[] locators);

    /**
     * Used to be named getInitializationScale(), but I renamed it so it can be re-used by functionality
     * without name confusion
     *
     * TODO adding a {@link ITimeInstant} parameter would allow for an agent's scale changing over time, which
     * is a possible design direction for Thinklab.
     *
     * @return
     */
    IScale getNonTemporallyDynamicScale();

    /**
     * Take in another scale and complete what's left of our specs by merging in its
     * details. E.g., we're a bounding box, we get a grid without extent, and we become
     * a grid in that bounding box. Will only be called during resolution, so the 
     * queries should have selected compatible scales, but throw an exception if
     * anything is not compatible.
     * 
     * @param scale
     * @return
     * @throws ThinklabException
     */
    IScale harmonize(IScale scale) throws ThinklabException;
}
