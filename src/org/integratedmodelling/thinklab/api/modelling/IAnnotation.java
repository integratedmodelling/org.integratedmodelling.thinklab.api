package org.integratedmodelling.thinklab.api.modelling;

import java.util.Map;

import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;

/**
 * Annotation (the Java kind, i.e. @ something). It's a function call syntactically, so
 * it gets the same interface, minus the call part.
 * 
 * @author Ferd
 *
 */
public interface IAnnotation extends ILanguageDefinition {

    /**
     * name given to a single parameter passed by itself, outside of a named list.
     */
    String DEFAULT_PARAMETER_NAME = "__default";

    public String getId();

    public IPrototype getPrototype();

    public Map<String, Object> getParameters();

    public INamespace getNamespace();

}
