package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * A topological object can be compared with topological operators to another of a 
 * compatible class.
 * 
 * TODO implement remaining contract from SFS or other
 * 
 * @author Ferdinando Villa
 *
 */
public interface ITopologicallyComparable<T> {

    /**
     * 
     * @param o
     * @return
     * @throws ThinklabException
     */
    public abstract boolean contains(T o) throws ThinklabException;

    /**
     * 
     * @param o
     * @return
     * @throws ThinklabException
     */
    public abstract boolean overlaps(T o) throws ThinklabException;

    /**
     * 
     * @param o
     * @return
     * @throws ThinklabException
     */
    public abstract boolean intersects(T o) throws ThinklabException;

    /**
     * FIXME this is in Topology<?> which I forgot about.
     * @param other
     * @return
     * @throws ThinklabException
     */
    public abstract ITopologicallyComparable<T> union(ITopologicallyComparable<?> other)
            throws ThinklabException;

    /**
     * FIXME this is in Topology<?> which I forgot about.
     * 
     * @param other
     * @return
     * @throws ThinklabException
     */
    public abstract ITopologicallyComparable<T> intersection(ITopologicallyComparable<?> other)
            throws ThinklabException;

    /**
     * Return a double that describes the extent of this topological object. It should only be
     * used to compare objects.
     * 
     * @return
     */
    public abstract double getCoveredExtent();

}
