package org.integratedmodelling.thinklab.api.modelling;

import java.util.Collection;

import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.lang.INamespaceQualified;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;

/**
 * @author Ferdinando
 *
 */
public interface IModelObject extends ILanguageDefinition, INamespaceQualified, IMetadataHolder {

    /**
     * 
     * @return
     */
    public abstract String getId();

    /**
     * true if flagged as namespace-private in the definition. For now only used in
     * models.
     * 
     * @return
     */
    public abstract boolean isPrivate();

    /**
     * If the object is inactive, it's not used for anything. Same specs as isPrivate(). 
     * 
     * @return
     */
    boolean isInactive();

    /**
     * 
     * @return
     */
    public abstract boolean hasErrors();

    /**
     * Any number of annotations may be added at runtime. These are Java-like annotations, not
     * semantic anything. 
     * 
     * @return
     */
    public abstract Collection<IAnnotation> getAnnotations();
}
