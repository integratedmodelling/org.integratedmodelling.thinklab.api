package org.integratedmodelling.thinklab.api.modelling.agents;

import org.integratedmodelling.thinklab.api.time.ITimeInstant;

public interface IObservationGraphNodeSubscriber {

    /**
     * invalidate any locally-held state (i.e. intermediate vector representation of an agent-state) as of the
     * interruptTime. This allows the collision mechanism in Thinklab to maintain consistency between the
     * (authoritative) Observation Graph and any cached and/or scale-mediated results which have been derived
     * from it.
     * 
     * @param interruptTime
     */
    public void invalidate(ITimeInstant interruptTime);

    /**
     * As agents progress forward in time, they update the Observation Graph, which then notifies all
     * listeners (i.e. scale mediators). This method handles one single state change, which is passed in as a
     * graph node containing agent state(s), state transition object, etc.
     * 
     * @param node
     */
    public void notify(IObservationGraphNode node);

}
