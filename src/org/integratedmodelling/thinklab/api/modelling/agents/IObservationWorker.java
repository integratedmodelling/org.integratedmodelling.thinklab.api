package org.integratedmodelling.thinklab.api.modelling.agents;

import org.integratedmodelling.exceptions.ThinklabException;

public interface IObservationWorker {

    /**
     * enter the main execution loop, pulling jobs from the dispatcher and processing them.
     * 
     * @throws ThinklabException
     */
    public void run() throws ThinklabException;

}
