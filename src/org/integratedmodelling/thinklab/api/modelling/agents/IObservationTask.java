package org.integratedmodelling.thinklab.api.modelling.agents;

import java.io.Serializable;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;

/**
 * A task item which will go on the Observation Queue. It can be as lightweight as possible -
 *
 * @author luke
 *
 */
public interface IObservationTask extends Comparable<IObservationTask>, Serializable {
    /**
     * Perform the observation
     * @param controller
     *
     * @throws ThinklabException
     */
    ITransition run() throws ThinklabException;

    /**
     * the instant in time in which the observation is made
     *
     * @return
     */
    ITimeInstant getObservationTime();

    /**
     * remember when work started, so that the currentlyProcessing queue can express staleness (not implemented yet).
     *
     * NOTE: this is set from the outside, so that any failure during task startup will not result in a lost task
     *
     * @param systemTimeMilliseconds
     */
    void startedWorkAt(long systemTimeMilliseconds);

    /**
     * get the time that work started (set by startedWorkAt())
     *
     * @return
     */
    long getStartTime();

    /**
     * set the task to be invalid, so that it will not be processed if pulled off the queue and no result will be saved
     * if one is returned by a worker.
     */
    void setInvalid();

    /**
     * get the validity state of the task (should it be processed? should a result be saved for it?)
     * @return
     */
    boolean isValid();

    public IObservationGraphNode getParentNode();

    void setParentNode(IObservationGraphNode node);

    ISubject getSubject();
}
