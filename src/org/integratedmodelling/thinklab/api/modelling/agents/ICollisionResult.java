package org.integratedmodelling.thinklab.api.modelling.agents;

public interface ICollisionResult {

    /**
     * TODO not currently in use. Should it be deleted? 
     * 
     * Did the collision result in the agent-state changing?
     * 
     * @return true if the agent-state was modified, either in value or in end time; false if the
     *         agent-state was left intact without any modification, and the next observation time is still
     *         valid for the agent
     */
    boolean stateChanged();

}
