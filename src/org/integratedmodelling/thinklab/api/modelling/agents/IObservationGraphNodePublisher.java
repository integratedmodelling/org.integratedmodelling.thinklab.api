package org.integratedmodelling.thinklab.api.modelling.agents;

import org.integratedmodelling.exceptions.ThinklabResourceNotFoundException;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;

public interface IObservationGraphNodePublisher {
    void subscribe(IObservationGraphNodeSubscriber listener, ISubject agent, ITimeInstant startTime)
            throws ThinklabResourceNotFoundException;
}
