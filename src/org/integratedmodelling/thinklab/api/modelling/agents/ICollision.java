package org.integratedmodelling.thinklab.api.modelling.agents;

import org.integratedmodelling.thinklab.api.time.ITimeInstant;

public interface ICollision {

    public ITimeInstant getCollisionTime();

    public IAgentState getCausingAgent();

    public IAgentState getImpactedAgent();

}
