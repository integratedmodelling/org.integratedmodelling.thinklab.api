package org.integratedmodelling.thinklab.api.modelling.agents;

import java.util.Collection;

import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;

public interface IObservationGraphNode {

    IAgentState getAgentState();

    ITransition getTransitionFromPrevious();

    IObservationTask getTaskCreatingThisNodeState();

    void addTaskCausedByThisNodeState(IObservationTask task);

    Collection<IObservationTask> getTasksCausedByThisNodeState();

    Collection<IScaleMediator> getSubscribers();

    boolean canCollideWithAnything();

}
