package org.integratedmodelling.thinklab.api.modelling.agents;

/**
 * A filter which is attached to an agent. An agent receives full-world information upon observation; this object
 * family will allow agents to "experience" only a subset of that information, and also to distort it.
 * 
 * For instance, a person sees in the visible spectrum up to a certain distance. Without this filter, a person
 * agent would have perfect vision for infinite distance, and could see all frequencies and could also see
 * through objects. This person would also see without any distortion due to perspective.
 * 
 * @author luke
 *
 */
public interface IInterpretationFilter {

}
