package org.integratedmodelling.thinklab.api.modelling.agents;

/**
 * TODO this might be pointless.
 * 
 * An optional intermediate step which a {@link IScaleMediator}
 * can cache to more efficiently generate data at the scale an observer expects it.
 * 
 * A Continuous Representation object could be used as an output value, too, provided it implements IState.
 * 
 * @author luke
 *
 */
public interface IContinuousRepresentation {

}
