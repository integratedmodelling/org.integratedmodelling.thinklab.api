package org.integratedmodelling.thinklab.api.modelling.agents;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabValidationException;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;

/**
 * Dispatcher for all observation tasks which need to happen in a simulation.
 *
 * For now, this interface, and its implementation in Thinklab, are just a simple wrapper around an
 * Observation queue and causal graph, but are intended to eventually become a server thread which worker
 * threads call to request observation tasks.
 *
 * @author luke
 *
 */
public interface IObservationController extends IObservationGraphNodePublisher {

    IObservationGraphNode createAgent(ISubject subject, IAgentState initialState,
            ITimePeriod initialTimePeriod, IObservationGraphNode parentNode, IObservationTask task,
            boolean enqueueSubsequentTask);

    /**
     * remove and return the head of the observation queue. Return null if the queue is empty.
     *
     * @return
     */
    public IObservationTask getNext();

    /**
     * after an observation has been done, set the result in the graph & agent-state map so that it is
     * readable by others. Also, by setting it in the graph, it is modifiable in the case of a collision; new
     * observation tasks can be created and executed to update the observation(s).
     *
     * @param task
     * @param result
     * @throws ThinklabValidationException
     */
    public void setResult(IObservationTask task, ITransition result) throws ThinklabValidationException;

    /**
     * This is how agents' temporal scales get disrupted from the outside. Agent states must be divided into
     * "before" and "after" the collision event, and agents must decide how to handle the collision, possibly
     * modifying the "after" state.
     *
     * @throws ThinklabException
     */
    public void collide(IObservationGraphNode agentState1, IObservationGraphNode agentState2,
            ICollision collision) throws ThinklabException;

    public void collide(IObservationGraphNode agentState, ICollision collision) throws ThinklabException;

}
