package org.integratedmodelling.thinklab.api.modelling.agents;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.IObjectState;
import org.integratedmodelling.thinklab.api.modelling.ISubject;
import org.integratedmodelling.thinklab.api.time.ITimeInstant;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;

/**
 * a temporary class to describe what is being called "agent-state" in the multiple-scale paper. It represents
 * a state (which can be a constant value, temporal function, or probability over either) valid for some time
 * period.
 * 
 * @author luke
 * 
 */
public interface IAgentState {

    ISubject getSubject();

    Map<IProperty, IObjectState> getStates();

    ITimePeriod getTimePeriod();

    /**
     * accept a collision time as a termination time. Normally, the only thing to do is set the end-time for
     * the agent-state, but this method allows agent-states to perform other cleanup or logic when this
     * happens.
     * 
     * NOTE: This is NOT where the agent processes a collision. The collision is considered to happen
     * "the moment immediately following" this agent-state end time.
     * 
     * @param interruptionTime
     * @return
     * @throws ThinklabException
     */
    IAgentState terminateEarly(ITimeInstant interruptionTime) throws ThinklabException;

}
