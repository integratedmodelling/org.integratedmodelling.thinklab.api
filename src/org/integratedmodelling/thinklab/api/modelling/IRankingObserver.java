package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.lang.RankingScale;

/**
 * Rankings are numerically ordered, linear quantification of a value that has no further
 * unit of measurement. The type returned by getStateType() will determine whether it's
 * a discrete or continuous ranking.
 *  
 * @author Ferd
 *
 */
public interface IRankingObserver extends INumericObserver {

    /**
     * A ranking may or may not be bound to a specific ranking scale. Only true
     * rankings (getType() == RANKING) may return a non-null scale.
     * 
     * @return
     */
    public abstract RankingScale getRankingScale();

}
