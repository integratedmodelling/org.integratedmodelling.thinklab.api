package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.thinklab.api.lang.IParseable;

/**
 * Represents a currency (not necessarily monetary) used by valuing observers and parsed
 * from language specifications.
 * 
 * @author Ferd
 *
 */
public interface ICurrency extends IParseable {

}
