package org.integratedmodelling.thinklab.api.modelling.knowledge;

import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.modelling.IModel;
import org.integratedmodelling.thinklab.api.modelling.IObserver;

/**
 * For tightness and efficiency, we define a specialized concept that describes the semantics of the observed
 * incarnation of another concept. It provides specialized and efficient access to the restrictions that
 * define the observation type and the inherent subject type (if any). If there is a model associated with the
 * observation (anywhere except in a resolved IState) the IObservable provides access to that as well.
 * 
 * If getSemantics() is called, the observable concepts reside in the observation namespace returned by
 * {@link IModelManager.getObservationNamespace()}.
 * 
 * @author Ferd
 * 
 */
public interface IObservable {

    /**
     * Return the unobserved type we're representing as an observation.
     * 
     * @return
     */
    IConcept getType();

    /**
     * Return the model we're using to interpret the observable. This is only defined in model observables,
     * and will return null in IStates.
     * 
     * @return
     */
    IModel getModel();

    /**
     * Return the observer that made this observation. It will normally return getModel().getObserver(), but
     * will be defined in IStates where getModel() returns null, and will return null when getModel() returns
     * a subject model. To investigate the semantics of the observation, getObservationType() should be used
     * instead of getObserver(), as that will be available in every situation, including past
     * serialization/deserialization.
     * 
     */
    IObserver getObserver();

    /**
     * Return the type of the observation that the un-observed type is restricted with. E.g. this may return
     * NS.RANKING if we're looking at a numeric way of observing the observable. For subject observations, it
     * will return NS.DIRECT_OBSERVATION. This will never return null.
     * 
     * @return
     */
    IConcept getObservationType();

    /**
     * Returns the type that the IObservable is inherent to. This correspond to the narrative description
     * "X of Y" and is defined for qualities, that can only exist inherently to something. The returned type
     * is usually a subject type (e.g. Elevation inherent to a Mountain) but it could be a quality (e.g.
     * Uncertainty inherent to Elevation). If no inherency has been defined for the observable, the return
     * type will be NS.PARTICULAR, as defined by DOLCE.
     * 
     * This will never return null.
     * 
     * @return
     */
    IConcept getInherentType();

    /**
     * An observable is always given a formal name that it can be referred to with. In models, this is the
     * formal name specified by the modeler. If no formal name is specified, a sensible and decent-looking
     * default should be provided.
     * 
     * @return
     */
    String getFormalName();

    /**
     * This returns the actual concept that incarnates the restrictions expressed above. It is only used for
     * "real" reasoning with a reasoner. Implementations should create that only at the first request, as
     * Thinklab can work effectively using just the functions above and without direct DL reasoning on
     * observation types.
     * 
     * @return
     */
    IConcept getSemanticType();

    /**
     * This will return a trait type if the observation is relative to a specific trait of the main type. It
     * will only return non-null in classified observable that classify 'by' a given trait. 
     * 
     * If not null, the matching of another observable (using the {@link IObservable.is(IObservable)} method) is done by 
     * checking that the passed observable inherits the trait in question.
     */
    IConcept getTraitType();

    /**
     * Main concept is abstract. Not really necessary - consider removing.
     * 
     * @return
     */
    boolean isAbstract();

    /**
     * Check identity of all concept known in both objects. All common concept must be what expected, no
     * concepts must be not in common. For traits, we check that the trait we contain, if any, is 
     * inherited by the passed observable.
     * 
     * @param concept
     * @return
     */
    boolean is(IObservable observable);

    /**
     * Name of either the main type or the delegate type, if any.
     * 
     * @return
     */
    String getLocalName();

    /**
     * Check identity of the main type only. Implies identity of the delegate type, which is forced to be a
     * subclass of it.
     * 
     * @param concept
     * @return
     */
    boolean is(IConcept concept);

    /**
     * Check identity of observation and observed type. Good to inquire whether e.g. we're quantifying or
     * classifying.
     * 
     * @param observationType
     * @param observedType
     * @return
     */
    boolean is(IConcept observationType, IConcept observedType);

    /**
     * Check identity of all types except trait.
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @return
     */
    boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType);

    /**
     * Namespace ID of either the main type or the delegate type, if any.
     * 
     * @return
     */
    String getConceptSpace();

    /**
     * True if an existing observation of this observable can be used to resolve the passed one.
     * @param o
     * @return
     */
    boolean canResolve(IObservable o);

    /**
     * Check identity of all types. Like passing the correspondent observable.
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @param traitType
     * @return
     */
    boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType, IConcept traitType);

}
