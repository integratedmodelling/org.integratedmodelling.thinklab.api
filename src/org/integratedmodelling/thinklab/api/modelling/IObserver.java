package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;

/**
 * An observer is the observation semantics of an observable concept. It is responsible for interpreting the
 * states that will describe the observable indirectly. It is only relevant for qualities - agents are
 * identifications so they don't have states or observers.
 * 
 * Each observer generates a concept in the ontology it belongs to - that concept may be named by the user
 * (with a model instruction) or be created when the modeled object resolves to a state (i.e. it's a
 * IDatasource). In all cases, Thinklab ensures that the semantics of the observation concept is consistent
 * and complete enough for validation of any further use in semantic mediation.
 * 
 * The concept returned by getObservableConcept() can be passed to {@link IModelManager.observes()} to check
 * what observation it makes.
 * 
 */
public abstract interface IObserver extends IObservingObject {

    /**
     * Return the accessor that will mediate to this observer. Do return one even if the mediation is
     * unnecessary or trivial - it will be simplified later. Only throw an exception if the observer passed is
     * incompatible.
     * 
     * @param observer
     * @param monitor report errors and warnings here - this is a runtime monitor that is different in each
     * session
     * @return
     */
    IStateAccessor getMediator(IObserver observer, IMonitor monitor) throws ThinklabException;

    /**
     * Return an accessor to interpret, not mediate, the data provided by the passed datasource accessor. This
     * means that the accessor will be linked to another not of the same type (i.e. a measurement will not be
     * linked to another measurement but to a raw data accessor). Called only when
     * canInterpretDirectly(accessor) returned true.
     * 
     * @param accessor
     * @param monitor report errors and warnings here - this is a runtime monitor that is different in each
     * session
     * @return
     */
    abstract IStateAccessor getInterpreter(IStateAccessor accessor, IMonitor monitor);

    /**
     * Called in observers whose {@link isComputing} returns true. These do not take any input directly, but
     * only know the dependencies stated in the observer definition. These are usually observers with actions
     * to be computed. This must return an accessor that will carry out the computations, without expecting to
     * have a state input for the main observable to modify.
     * 
     * @param monitor report errors and warnings here - this is a runtime monitor that is different in each
     * session
     * 
     * @return
     * @throws ThinklabException
     */
    abstract IStateAccessor getComputingAccessor(IMonitor monitor) throws ThinklabException;

    /**
     * Get the observer we are mediating if any.
     * 
     * @return
     */
    abstract IObserver getMediatedObserver();

    /**
     * Passed the accessor from a downstream datasource that we are asked to provide observation semantics
     * for. If this returns true, the datasource accessor is used unmodified. Otherwise, the observer is asked
     * to provide an accessor by calling {@link getInterpreter} and a mediation link is used to join the two
     * in the resulting workflow. In general practice only numeric and string observers without actions should
     * respond true, but special accessor types may require more sophistication.
     * 
     * @param accessor
     * @return
     */
    abstract boolean canInterpretDirectly(IAccessor accessor);

    /**
     * Returns the core type that defines the observation this observer is making and how it matches the final
     * observable. The type should be specialized enough to allow correct reasoning and matching; for example,
     * for measurements it should specialize to include the physical dimensions of the observer (e.g.
     * LengthMeasurement) so that we can lookup compatible observations. It will end up in the IObservable
     * returned by getObservable().
     * 
     * @param namespace the namespace where to create the concept if it's necessary to create one.
     * 
     * @return
     */
    public abstract IConcept getObservationType(INamespace namespace);

    /**
     * Train the model to match any output state that can be observed in the kbox/context. Details of how to
     * choose or subset the output states are left to the implementation. Not all observer will be trainable -
     * if not trainable, this one should return the same model (with the option of logging a warning).
     * 
     * It should never modify the observer it's called on - it would be a const if there was such a thing in
     * Java.
     * @param monitor TODO
     * @param params
     * @return a new trained model that has learned to reproduce the output states observed on the passed
     * kbox.
     * @throws ThinklabException
     */
    IObserver train(ISubject context, IMonitor monitor) throws ThinklabException;

    /**
     * Return the model we're part of. Tried to avoid this forever, but visualization made it necessary.
     * 
     * @return
     */
    IModel getModel();

    /**
     * Redefine the observable concept if necessary, i.e. if the observer changes the semantics of the type
     * it's declared for. This will apply for example to counts, presence and any other that turn a subject
     * into a quality. Others will just return the concept passed.
     * 
     * @param concept
     * @return
     */
    IConcept getObservedType(IConcept concept);

    /**
     * If this is true, the observer describes an integrated quality over the explicit scale given in the
     * definition (e.g. kgs of carbon per "cell" instead of per unit of area). Mediation and user validation
     * must use this information along with the semantics of the observable and the extent being observed to \
     * ensure correct results.
     * 
     * @return
     */
    boolean isIntegrated();

    /**
     * True if there is an accessor. 
     * 
     * @return
     */
    boolean hasAccessor();
}
