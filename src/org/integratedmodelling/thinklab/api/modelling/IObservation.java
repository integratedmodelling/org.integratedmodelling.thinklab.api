package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.thinklab.api.knowledge.ISemanticObject;
import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;

/**
 * Anything that has been observed. Currently IState for qualities and ISubject for
 * things. The only restrictions it places on a regular semantic object (for now) is
 * that the direct type must be an IObservable, which carries the observation 
 * semantics with it.
 * 
 * @author Ferd
 *
 */
public interface IObservation extends ISemanticObject<Object> {

    /**
     * Restricts {@link ISemanticLiteral.getDirectType()} to return a {@link IObservable} 
     * instead of a raw IConcept.
     */
    public IObservable getObservable();

    /**
     * Return the scale seen by this object, merging all the extents declared
     * for the subject in the observation context.
     *
     * @return
     */
    public IScale getScale();
}
