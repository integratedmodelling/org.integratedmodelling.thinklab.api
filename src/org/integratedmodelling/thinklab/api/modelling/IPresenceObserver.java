package org.integratedmodelling.thinklab.api.modelling;

/**
 * Presence/absence observer produces true/false states. No specific methods for
 * now. The booleans can be automatically conceptualized based on the observable
 * when in a semantic context.
 *  
 * @author Ferd
 *
 */
public interface IPresenceObserver extends IObserver {

}
