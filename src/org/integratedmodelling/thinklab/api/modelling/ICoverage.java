package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * An object that represents the total coverage for a subject
 * after resolution in a context.
 * 
 * @author Ferd
 *
 */
public interface ICoverage {

    /**
     * Return the proportion of total coverage as a double 0-1. 
     * @return
     */
    Double getCoverage();

    /**
     * 
     * Union of the coverages. NOTE: this will not unite the passed coverage if the ADDITIONAL coverage resulting
     * from the union is less than the proportion returned by isRelevant() (20% by default). The proportion of coverage should
     * be checked after or() to see if anything has changed.
     * 
     * @param coverage
     * @param minAcceptedImprovement
     * @return
     * @throws ThinklabException
     */
    ICoverage or(ICoverage coverage) throws ThinklabException;

    /**
     * 
     * @param coverage
     * @return
     * @throws ThinklabException
     */
    ICoverage and(ICoverage coverage) throws ThinklabException;

    /**
     * True if the coverage is less than the global setting defining a
     * usable coverage (default 1%).
     * 
     * @return
     */
    boolean isEmpty();

    /**
     * true if the coverage is at least as much as the minimum required coverage of a context (95% by
     * default). Note that setting this to 1.0 may trigger lots of resolutions to resolve minute portions
     * of the context.
     * 
     * @return
     */
    boolean isComplete();

    /**
     * true if the coverage is relevant enough for a model to be accepted by the resolver (default
     * smallest extent intersection covers 25% of scale).
     * 
     * @return
     */
    boolean isRelevant();

}
