package org.integratedmodelling.thinklab.api.modelling;

/**
 * Rankings are numerically ordered, linear quantification of a value that has no further
 * unit of measurement. The type returned by getStateType() will determine whether it's
 * a discrete or continuous ranking.
 *  
 * @author Ferd
 *
 */
public interface IProbabilityObserver extends INumericObserver {

}
