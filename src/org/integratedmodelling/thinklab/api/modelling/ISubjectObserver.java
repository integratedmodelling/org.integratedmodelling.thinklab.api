//package org.integratedmodelling.thinklab.api.modelling;
//
//import java.util.Collection;
//
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.thinklab.api.knowledge.IProperty;
//import org.integratedmodelling.thinklab.api.listeners.IMonitor;
//import org.integratedmodelling.thinklab.api.modelling.knowledge.IObservable;
//import org.integratedmodelling.thinklab.api.modelling.resolution.IResolutionStrategy;
//import org.integratedmodelling.thinklab.api.modelling.resolution.ISubjectResolver;
//import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
//
///**
// * A subject observer is created by a subject generator to wrap a subject (instance of ISubject) and provide 
// * API control of
// * the strategy used to observe it. When created, the ISubject instance is in an uninitialized state and no
// * model strategy has been defined to observe it. Calling resolve() will attempt to create the best
// * observation strategy and return the total context coverage relative to the subject represented. As
// * a result of resolution, the subject's scale may become different from the one initially set. If the
// * coverage is not empty, the wrapped ISubject will have been initialized by running the initialization
// * workflow. The latter can be inspected using the IProvenance and IWorkflow graphs created for it.
// * 
// * If the coverage is not empty, run() can be called to complete the observation, running any actions that the
// * semantics of the subject and its children imply. run() does nothing but returning the initialized subject
// * if the original instance does not know time. The result of run() is the fully observed subject.
// * 
// * The addObservation() method produces another ISubjectObserver that will observe further objects (concepts
// * or models) in the context of the same root subject.
// * 
// * @author Ferd
// * @deprecated we can do without this - just use a Subject; it has an initialize() and contextualize() (run) method that resolves it; 
// * dependencies go in the subject, initialize() uses the presence of a model to define whether that's necessary, then 
// * resolves that first, then all dependencies in groups.
// */
//public interface ISubjectObserver extends ISubjectResolver {
//
//    /**
//     * Resolve the associated subject to an observation strategy using the model base and observe all
//     * associated qualities and subjects to their initialized state. Return the total coverage of the context
//     * that can be observed.
//     * 
//     * @param scenarios a list of scenario names to use in resolution, or null.
//     * 
//     * @return
//     * @throws ThinklabException
//     */
//    ICoverage initialize(Collection<String> scenarios) throws ThinklabException;
//
//    /**
//     * Called after initialize() has returned a non-empty ICoverage, it will create a schedule for the subject
//     * hierarchy and run it. This is the only function that returns an ISubject, which is immutable and can
//     * only be the result of observation.
//     * 
//     * @return
//     * @throws ThinklabException
//     */
//    ISubject run() throws ThinklabException;
//
//    /**
//     * Return another observer that will observe the passed object in the context of the subject we are
//     * associated with. The ISubjectObserver returned can be resolved to obtain a new observation strategy
//     * including the new object.
//     * 
//     * @param observable a concept or model to be observed in our context
//     * @param property a property to link the new observation to our subject
//     * @param monitor monitor for the operation, or null.
//     * @return
//     * @throws ThinklabException
//     */
//    ISubjectObserver addObservation(IObservable observable, IProperty property, IMonitor monitor)
//            throws ThinklabException;
//
//    /**
//     * Can be called after each resolve() to retrieve the strategy inferred by the resolver. This will be (for now)
//     * composed of a provenance graph and a dataflow - more may be added later.
//     * 
//     * @return
//     */
//    IResolutionStrategy getResolutionStrategy();
//
//    /**
//     * Return the root subject wrapped by this observer, without modifying it or running anything
//     * @return
//     */
//    ISubject getSubject();
//
//    /**
//     * Added by Luke - this is just an approximation of how this could work. The SubjectObserver.run() method
//     * would observe a subject, returning an observed instance of an ISubject to the ObservationTask which
//     * called it. The ObservationTask would then call ISubject.performTemporalTransitionForCurrentState() so
//     * that any {@link ScaleMediator}s which are observing the subject could apply a diff, instead of
//     * re-interpolating from scratch.
//     * 
//     * @return
//     * @throws ThinklabException
//     */
//    ITransition performTemporalTransitionFromCurrentState();
//
// }
