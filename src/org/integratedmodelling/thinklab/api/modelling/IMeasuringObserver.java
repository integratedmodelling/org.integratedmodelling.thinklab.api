package org.integratedmodelling.thinklab.api.modelling;

public interface IMeasuringObserver extends INumericObserver {

    public abstract IUnit getUnit();

}
