/**
 * IExtent.java
 * ----------------------------------------------------------------------------------
 * 
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of ThinklabCoreSciencePlugin.
 * 
 * ThinklabCoreSciencePlugin is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * ThinklabCoreSciencePlugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * ----------------------------------------------------------------------------------
 * 
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.api.modelling;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;

/**
 * An Extent describes the topology of the observable
 * it's linked to. Conceptual models are capable of producing unions or intersections
 * of extents.
 * 
 * Extents must be conceptualizable and the result of conceptualizing
 * them must be an observation describing the extent. They are also expected
 * to implement equals() and hash() in a fast way (e.g. by using signatures).
 * 
 * @author Ferdinando Villa
 *
 */
public abstract interface IExtent extends IState, ITopology<IExtent> {

    /**
     * Return the main concept for the topological class represented by this extent. It should
     * be the same concept for all the different kinds of extents representing the same domain,
     * i.e. geospace:Space, which should be an ancestor to the observable of this
     * extent. It will be used to ensure that no two extents of the same domain concept
     * appear in a context. The context implementation is expected to try to merge extents
     * that share the same domain concept even if their observables are not the same.
     * 
     * @return
     */
    public abstract IConcept getDomainConcept();

    /**
     * Return the property that should link this extent to a ISubject. It should be
     * a data property.
     * 
     * @return
     */
    public abstract IProperty getDomainProperty();

    /**
     * Get the property that should link this extent to a ITopologicallyComparable object
     * describing the extent of its coverage.
     * 
     * @return
     */
    public IProperty getCoverageProperty();

    /**
     * Collapse the multiplicity and return the extent that represents
     * the full extent of our topology in one single state. This extent may
     * not necessarily be of the same class.
     * 
     * @return a new extent with getValueCount() == 1.
     */
    public IExtent collapse();

    /**
     * Return the n-th state of the ordered topology as a new extent with one
     * state.
     * 
     * @param granule 
     * @return a new extent with getValueCount() == 1.
     */
    public IExtent getExtent(int stateIndex);

    /**
     * Return the single-valued topological value that represents the total extent covered, ignoring
     * the subdivisions.
     * 
     * @return
     */
    public ITopologicallyComparable<?> getExtent();

    /**
     * True if the i-th state of the topology correspond to a concrete subdivision where
     * observations can be made. Determines the status of "data" vs. "no-data"
     * for the state of an observation defined over this extent.
     * 
     * @param granule
     * @return whether there is an observable world at the given location.
     */
    public abstract boolean isCovered(int stateIndex);

    /**
     * Return an extent of the same domainConcept that represents the merge
     * of the two. If force is false, this operation should just complete
     * the semantics - e.g. add a shape to a grid or such - and complain
     * if things are incompatible.
     * 
     * If force is true, return an extent that is capable of representing the passed one through
     * the "lens" of our semantics. Return null if the operation is legal but
     * it results in no context, throw an exception if we don't know what to
     * do with the passed context.
     * 
     * 
     * @param extent
     * @return
     * @throws ThinklabException
     */
    public IExtent merge(IExtent extent, boolean force) throws ThinklabException;

    /**
     * Try to cover the extent with the passed object and return the resulting
     * coverage object and the proportion of the total extent covered by it.
     * 
     * @param obj
     * @return
     * @throws ThinklabException 
     */
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws ThinklabException;

    /**
     * True if the extent is completely specified and usable. Extents may
     * be partially specified to constrain observation to specific representations
     * or scales.
     * 
     * @return
     */
    public boolean isConsistent();

    /**
     * Return true if this extent covers nothing.
     * @return
     */
    public abstract boolean isEmpty();

    /**
     * Each extent may have > 1 inner dimensions. If so, the linear offset (0 .. getMultiplicity())
     * will be mapped to them according to their size and order. This one returns the full
     * internal dimensionality. If the extent is one-dimensional, it will 
     * return <code>new int[] { getMultiplicity() }</code>.
     * 
     * @param rowFirst if true, endeavor to return offset in the order that most closely
     *        resembles row-first ordering wrt euclidean x,y,z (default).
     *        
     * @return
     */
    public abstract int[] getDimensionSizes(boolean rowFirst);

    /**
     * Translate a linear offset into the offsets for each dimension. If the dimension is 1,
     * return the offset itself. 
     * 
     * @param linearOffset
     * @param rowFirst if true, endeavor to return offset in the order that most closely
     *        resembles row-first ordering wrt euclidean x,y,z (default).
     * @return
     */
    int[] getDimensionOffsets(int linearOffset, boolean rowFirst);

}
