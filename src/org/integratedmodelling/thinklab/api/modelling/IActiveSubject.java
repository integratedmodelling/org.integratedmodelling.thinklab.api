package org.integratedmodelling.thinklab.api.modelling;

import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IProperty;
import org.integratedmodelling.thinklab.api.modelling.agents.IAgentState;
import org.integratedmodelling.thinklab.api.modelling.agents.ICollision;
import org.integratedmodelling.thinklab.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.thinklab.api.modelling.scheduling.ITransition;
import org.integratedmodelling.thinklab.api.time.ITimePeriod;

/**
 * The in-memory version of a ISubject, capable of "doing" things. 
 * 
 * @author Ferd
 *
 */
public interface IActiveSubject extends ISubject {

    /**
     * Return the linked states as a map keyed by the properties that are represented
     * by each state. Only return the IObjectState states, because that is how
     * these results are used in calling code. (abstract states
     * like topologies are not relevant to callers)
     * @return
     */
    public Map<IProperty, IObjectState> getObjectStateCopy();

    /**
     * Added by Luke - this is where an agent determines whether or not two agent-states cause a collision. 
     * It is left to each agent (i.e. processing is done twice for every pair of overlapping
     * agent-states) to decide whether a collision happens, and why, and when, and what is the result. This is
     * because an agent might have a subjective idea of what types of collisions might happen, might have
     * domain-specific knowledge or detection algorithms, etc.
     *
     * This should always return the FIRST collision in the overlapping time period if there are more than
     * one, because the two overlapping agents' results (collisions) will be compared and the LATER one will
     * be discarded.
     *
     * @param myAgentState
     * @param otherAgentState
     * @return
     */
    ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState);

    /**
     * Added by Luke - this is where an agent will determine whether or not its agent-state will be affected
     * by the collision. If it answers true, then the agent-state will be partitioned into an
     * un-affected pre-collision partition, and a new observation task will be created for the collision time
     * which takes the collision into consideration. The post-collision agent-state may or may not be changed
     * from the original, and its duration may be the same or different.
     *
     * All causal relationships which depended on this agent-state AFTER the collision time will be
     * invalidated and so must be re-observed. This implies that the agent must re-create any causal links
     * (and observation tasks) which are a result of the new post-collision agent-state.
     *
     * @param agentState
     * @param collision
     * @return
     */
    boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision);

    /**
     * Adds a computed state, determining the property to link with using the semantics. Throw an exception if
     * adding the state would result in semantic inconsistency.
     * 
     * @param s
     * @throws ThinklabException
     */
    void addState(IState s) throws ThinklabException;

    /*
     * set the scale to the final harmonized one resulting from resolution.
     */
    void setScale(IScale s);

    void removeStates();

    /**
     * Adds an initialized subject, determining the property to link with using the semantics. Throw an exception if
     * adding the subject would result in semantic inconsistency.
     * 
     * @param s
     * @throws ThinklabException
     */

    void addSubject(ISubject ds) throws ThinklabException;

    ITransition reEvaluateStates(ITimePeriod timePeriod);

    ITransition performTemporalTransitionFromCurrentState();
}
