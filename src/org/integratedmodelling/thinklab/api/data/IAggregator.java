package org.integratedmodelling.thinklab.api.data;

import java.util.Collection;

/**
 * Used in table operations
 * @author Ferd
 *
 */
public interface IAggregator {
    public Object aggregate(Collection<Object> objects);
}
