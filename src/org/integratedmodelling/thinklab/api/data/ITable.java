package org.integratedmodelling.thinklab.api.data;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.knowledge.IExpression;
import org.integratedmodelling.thinklab.api.modelling.parsing.ILanguageDefinition;

public interface ITable extends ILanguageDefinition {

    /**
     * Table name. E.g. a sheet name in Excel.
     * 
     * @return
     */
    String getName();

    /**
     * Return all columns
     * 
     * @return
     */
    Collection<IColumn> getColumns();

    /**
     * Return the full column with the passed ID.
     * 
     * @param columnName
     * @return
     */
    IColumn getColumn(String columnName);

    /**
     * Lookup values in columnIndex based on matching the other values. If other values are expressions,
     * run them with the value of each column ID in the current row as parameters. Values that are not
     * expressions are matched to columns in left to right order, skipping the requested result column.
     * 
     * @param columnId the index of the column we want returned
     * @param values values or expressions to be matched to the other columns, left to right.
     * @return
     */
    List<Object> lookup(int columnId, Object... values);

    /**
     * Lookup values in columnIndex based on matching the other values. If other values are expressions,
     * run them with the value of each column ID in the current row as parameters. Values that are not
     * expressions are matched to columns in left to right order, skipping the requested result column.
     * 
     * @param columnId the ID of the column we want returned
     * @param values values or expressions to be matched to the other columns, left to right.
     * @param parameters additional parameters for the expression evaluation.
     * 
     * @return
     * @throws ThinklabException 
     */
    List<Object> lookup(String columnId, IExpression match, Map<String, Object> parameters)
            throws ThinklabException;

    /**
     * Return all the rows that match IExpression, which may use any of the column header IDs.
     * 
     * @param expression
     * @return
     */
    List<Map<String, Object>> lookup(IExpression expression);

}
