package org.integratedmodelling.thinklab.api.data;

import java.util.List;

/**
 * 
 * @author Ferd
 *
 */
public interface IColumn {

    String getName();

    int getValueCount();

    List<Object> getValues();

    Object getValue(Object index);

}
