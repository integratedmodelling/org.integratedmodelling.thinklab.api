package org.integratedmodelling.thinklab.api.data;

public interface IDataAsset {

    // types - extendible of course
    public final static String TYPE_WFS      = "wfs";
    public final static String TYPE_WCS      = "wcs";
    public final static String TYPE_RASTER   = "raster";
    public final static String TYPE_VECTOR   = "vector";
    public final static String TYPE_OPENDAP  = "opendap";
    public final static String TYPE_INLINE   = "inline";
    public final static String TYPE_TABLE    = "table";

    // data keys - not all used, more will be needed
    public final static String KEY_URL       = "url";
    public final static String KEY_DATAID    = "dataid";
    public final static String KEY_USER      = "user";
    public final static String KEY_PASSWORD  = "password";
    public final static String KEY_ATTRIBUTE = "attribute";
    public final static String KEY_VALUE     = "value";
    public final static String KEY_TABLE     = "table";
    public final static String KEY_FILTER    = "filter";

    String getUrn();

    String getType();

    String getAttribute(String key);
}
