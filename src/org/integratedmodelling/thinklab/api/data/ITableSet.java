package org.integratedmodelling.thinklab.api.data;

import java.util.Collection;

public interface ITableSet {

    String getName();

    Collection<ITable> getTables();

    ITable getTable(String tableName);

}
