package org.integratedmodelling.thinklab.api.configuration;

import java.io.File;
import java.util.Properties;

import org.integratedmodelling.collections.OS;
import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.Version;

/**
 * This interface tags objects that have configuration properties and need workspace areas, such as sessions,
 * project and plugins. Objects implementing this interface can have installation directories and must have
 * workspaces, temp areas and scratch areas.
 * 
 * @author Ferd
 * 
 */
public interface IConfiguration {

    // use these subspace names for extra consistency.
    public final static String SUBSPACE_CONFIG                  = "config";
    public final static String SUBSPACE_KNOWLEDGE               = "knowledge";
    public final static String SUBSPACE_SOURCE                  = "src";
    public final static String SUBSPACE_PLUGINS                 = "plugins";
    public final static String SUBSPACE_LIB                     = "lib";
    public final static String SUBSPACE_PROJECTS                = "deploy";
    public final static String SUBSPACE_INDEX                   = "index";

    /*
     * properties that can be set to define what states to store
     */
    public static final String STORE_RAW_DATA_PROPERTY          = "thinklab.store.raw";
    public static final String STORE_INTERMEDIATE_DATA_PROPERTY = "thinklab.store.intermediate";
    public static final String STORE_CONDITIONAL_DATA_PROPERTY  = "thinklab.store.conditional";
    public static final String STORE_MEDIATED_DATA_PROPERTY     = "thinklab.store.mediated";
    public static final String THINKLAB_PROJECT_DIR_PROPERTY    = "thinklab.project.dir";

    /**
     * Configurable objects should have properties that can be persisted across invocations.
     * 
     * @return
     */
    public Properties getProperties();

    /**
     * Return a default workspace area where things of general use may be written. A Workspace is a known
     * place that should contain things that are known to the user and understandable, such as project files.
     * Use a ScratchArea for caches and strangeness that users shouldn't see.
     * 
     * @return a valid directory. This should always return a valid, existing and writable path.
     */
    public abstract File getWorkspace();

    /**
     * Return a workspace area where things relevant to the given subspace may be written. A Workspace is a
     * known place that should contain things that are known to the user and understandable, such as project
     * files. Use a ScratchArea for caches and strangeness that users shouldn't see.
     * 
     * @return a valid directory. This should always return a valid, existing and writable path.
     */
    public abstract File getWorkspace(String subspace);

    /**
     * the data path is the root of the other system-oriented paths, which are assumed not to be relevant to
     * the user. Used by both client and server.
     * 
     * @return
     */
    File getDataPath();

    /**
     * A scratch area is for places where stuff that's not going to be understandable to the user will get
     * written. It should remain there across sessions.
     * 
     * @return a valid, writable scratch path.
     */
    public abstract File getScratchArea();

    /**
     * A scratch area is for places where stuff that's not going to be understandable to the user will get
     * written. It should remain there across sessions. Use this one for specific subdirectories.
     * 
     * @return a valid, writable scratch path.
     */
    public abstract File getScratchArea(String subArea);

    /**
     * A temp area is like a scratch path but the object implementing this configuration should ensure that
     * it's removed either at the end of a session or at the beginning of a new one, ideally at both points.
     * 
     * @param subArea
     * @return
     */
    public abstract File getTempArea(String subArea);

    /**
     * Should be tied to the build mechanism.
     * 
     * @return
     */
    public Version getVersion();

    public File getProjectDirectory(String id);

    public File getProjectDirectory();

    public void persistProperties() throws ThinklabException;

    public OS getOS();

    /**
     * Return a global notification level, using the INotification constants.
     * @return
     */
    public int getNotificationLevel();
}
