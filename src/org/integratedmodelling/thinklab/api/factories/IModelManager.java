package org.integratedmodelling.thinklab.api.factories;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.exceptions.ThinklabIOException;
import org.integratedmodelling.thinklab.api.knowledge.IConcept;
import org.integratedmodelling.thinklab.api.lang.IModelResolver;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.modelling.IModelObject;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.modelling.ISubject;

/**
 * The model factory contains the register of all model objects and namespaces. It's capable of
 * reconstructing dependency structure and source code of all objects. It can load model definitions
 * from resources and release objects on a namespace basis.
 * 
 * The paradigm "one resource, one namespace" is mandatory and enforced.
 * 
 * The namespaces handled by the model manager are not the only namespaces in the system. Core
 * knowledge not loaded from projects is not seen by the model manager. The Knowledge manager 
 * sees every namespace. Also it must be able to provide a privileged namespace where all
 * model observables are created.
 * 
 * @author Ferdinando
 *
 */
public interface IModelManager {

    /**
     * 
     * @param ns
     * @return
     */
    INamespace getNamespace(String ns);

    /**
     * 
     * @param namespace
     */
    void releaseNamespace(String namespace);

    /**
     * Return all namespaces.
     * @return
     */
    Collection<INamespace> getNamespaces();

    /**
     * Return all namespaces tagged as scenarios.
     * 
     * @return
     */
    List<INamespace> getScenarios();

    //    /**
    //     * Return a context that describes the coverage of a particular
    //     * model in a given kbox along all applicable dimensions. 
    //     * It will be the union of the contexts of
    //     * all the realized models found in that kbox. 
    //     *
    //     * @param model
    //     * @return
    //     */
    //    IScale getCoverage(IModel model);

    /**
     * Load all model objects defined in the given file, adding them to the model kbox as appropriate.
     * 
     * @param resourceId
     * @param namespaceId 
     * @param monitor 
     * @param the higher-level resolver (normally a project resolver obtained by a call to IResolver.getProjectResolver())
     * @param set of the namespace IDs already loaded in this session, so that no recursive loading of dependencies is done. Start empty.
     * @return the namespace defined
     * @throws ThinklabIOException 
     */
    INamespace loadFile(final File resourceId, final String namespaceId, IModelResolver resolver,
            boolean substituteExisting, IMonitor monitor, Set<String> context) throws ThinklabException;

    /**
     * Lookup the model object identified by this ID. For now only lookup
     * those in the NSs loaded directly as files. Later we can query the kbox too.
     * 
     * @param name
     * @return
     */
    IModelObject findModelObject(String name);

    /**
     * Return the observation namespace where all observable concepts reside. It must always return
     * the same non-null namespace. It's created and used at runtime only, so it starts empty and 
     * doesn't need to be written anywhere.
     * 
     * @return
     */
    INamespace getObservationNamespace();

    /**
     * Return the root resolver. This is normally a single instance per manager.
     * The normal usage pattern is to ask for a context resolver from it, which is then 
     * used to resolve groups of projects and/or namespaces.
     * 
     * @return 
     * 
     */
    IModelResolver getRootResolver();

    /**
     * Check if this is a file that we can turn into a namespace. Normally it
     * will be based on extension checking, but feel free to make it smarter (e.g.
     * only accepting files from a set of trusted locations).
     * 
     * @param f
     * @return
     */
    boolean isModelFile(File f);

    /**
     * Return the function prototype for the given function ID, or null if not registered.
     * 
     * @param id
     * @return
     */
    public abstract IPrototype getFunctionPrototype(String id);

    /**
     * Associate a subject class to a concept and its subclasses (until overridden by
     * another association). 
     * 
     * @param concept
     * @param cls
     */
    void registerSubjectClass(String concept, Class<? extends ISubject> cls);

    /**
     * Return the Java class that incarnates the passed subject type.
     * 
     * @param type
     * @return
     */
    Class<? extends ISubject> getSubjectClass(IConcept type);

}
