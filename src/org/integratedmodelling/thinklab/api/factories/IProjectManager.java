package org.integratedmodelling.thinklab.api.factories;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.listeners.IMonitor;
import org.integratedmodelling.thinklab.api.listeners.IProjectLifecycleListener;
import org.integratedmodelling.thinklab.api.modelling.INamespace;
import org.integratedmodelling.thinklab.api.project.IDependencyGraph;
import org.integratedmodelling.thinklab.api.project.IProject;

/**
 * The server side of project management. Projects define namespaces, containing knowledge - either
 * ontologies or model objects, plus any support resources necessary.
 * 
 * @author Ferd
 *
 */
public interface IProjectManager {

    /**
     * Add a listener so project activity can be monitored.
     * 
     * @param listener
     */
    public void addListener(IProjectLifecycleListener listener);

    /**
     * Return a specific project if registered, or null if not found. Do not throw an 
     * exception. The project may be loaded or unloaded.
     * 
     * @param projectId
     * @return
     */
    public IProject getProject(String projectId);

    /**
     * Return all the projects registered with the manager. They may or may not be
     * loaded.
     * 
     * @return
     */
    public Collection<IProject> getProjects();

    /**
     * Load every project registered in order of dependency, minimizing unnecessary unloading
     * unless the force option is true.
     * 
     * Return all the namespaces that were read anew - do not return those that were loaded and
     * had untouched sources or were read-only.
     * 
     * @throws ThinklabException
     */
    List<INamespace> load(boolean forceReload, IMonitor monitor) throws ThinklabException;

    /**
     * Deploy a project contained in a given archive or directory.
     * 
     * @param pluginId
     * @param resourceId
     * @return
     * @throws ThinklabException
     */
    IProject deployProject(String pluginId, String resourceId, IMonitor monitor) throws ThinklabException;

    /**
     * Synonymous of unload followed by unregister.
     * 
     * @param projectId
     * @throws ThinklabException
     */
    void undeployProject(String projectId) throws ThinklabException;

    /**
     * Register a project contained in the passed directory. If the directory does not
     * contain a project, throw an exception. If the project is already registered, do
     * nothing. Return a list of the IDs of all projects that were not already registered.
     * Do not load the projects.
     * 
     * @param projectDir
     * @return
     * @throws ThinklabException 
     */
    List<String> registerProject(File... projectDir) throws ThinklabException;

    /**
     * Load a project, ensuring all its prerequisites are also loaded. This should work as
     * refresh() as well, but not throw an exception if the project isn't loaded. Return
     * the list of namespaces that were actually read - i.e., do not return namespaces
     * that were already loaded and needed no refresh because their source files 
     * had not changed. 
     * 
     * @param projectId
     * @return 
     * @throws ThinklabException
     */
    List<INamespace> loadProject(String projectId, IMonitor monitor) throws ThinklabException;

    /**
     * Unregister the project, unloading it (and those that depend on it) if loaded. After this
     * is called, load(id) will throw an exception.
     * 
     * @param projectId
     */
    void unregisterProject(String projectId);

    /**
     * The project manager should not have a watched project directory by default - projects
     * directories can be registered individually. If this is given, it should register all
     * the projects in it.
     *  
     * @param projectDirectory
     * @throws ThinklabException 
     */
    void registerProjectDirectory(File projectDirectory) throws ThinklabException;

    /**
     * Unload the project. Should count references to the project and leave its definitions
     * in the knowledge base unless no other loaded projects reference it.
     * 
     * @param projectId
     * @throws ThinklabException
     */
    void unloadProject(String projectId) throws ThinklabException;

    /**
     * Get the up to date dependency graph. Should never be used in a circumstance when
     * load() could be called concurrently.
     * 
     * @return
     */
    IDependencyGraph getDependencyGraph();

    /**
     * true if load() has been called at least once. If this returns false, the model manager is
     * probably unaware of what the project space contains.
     * 
     * @return
     */
    boolean hasBeenLoaded();

}
