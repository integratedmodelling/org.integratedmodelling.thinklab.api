package org.integratedmodelling.thinklab.api.runtime;

import java.util.Collection;
import java.util.List;

/**
 * Serializable object that contains the assets for a user: the core knowledge that
 * is loaded at authorization, the projects the user can access, etc.
 * 
 * @author Ferd
 *
 */
public interface IUserAssets {

    /**
     * Types of assets. TODO flesh out.
     */
    String TYPE_PROJECT     = "project";
    String TYPE_DATA_RECORD = "datarecord";

    /**
     * Return the list of project names that contain the core knowledge for
     * this user, in the order in which they must be loaded.
     * 
     * @return
     */
    public List<String> getCoreProjectNames();

    /**
     * Return all the project this user has access to. These will depend on
     * the user's groups and access levels, which are negotiated independently.
     * 
     * @return
     */
    public Collection<String> getAllowedProjectNames();

    /**
     * Get the URLs of all the servers this user has the ability of accessing. Each
     * server will return capabilities after authentication.
     * 
     * @return
     */
    public Collection<String> getAllowedServers();

}
