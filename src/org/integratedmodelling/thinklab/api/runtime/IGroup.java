package org.integratedmodelling.thinklab.api.runtime;

import java.util.Collection;

/**
 * Users may belong to one or more of these.
 * 
 * @author Ferd
 *
 */
public interface IGroup {

    /**
     * Group ID - customarily a short identifier in uppercase.
     * @return
     */
    String getId();

    /**
     * Primary server URL. Can be null and should be in all but the one group that defines
     * the server that handles it.
     * @return
     */
    String getPrimaryServer();

    /**
     * Core projects, downloaded and synchronized from the primary server to each client at startup.
     * @return
     */
    Collection<String> getCoreProjects();

    /**
     * Searched project, not downloaded but enabled for knowledge search and retrieval for users
     * belonging to this group.
     * 
     * @return
     */
    Collection<String> getSearchedProjects();
    
    /**
     * Get URLs of the model servers available for the group. Should have more info like whether the
     * group can search, run models and lock - perhaps as a concatenated string for now.
     * 
     * @return
     */
    Collection<String> getModelServers();

}
