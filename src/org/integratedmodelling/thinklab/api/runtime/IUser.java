package org.integratedmodelling.thinklab.api.runtime;

import java.io.File;
import java.util.Date;
import java.util.Set;

/**
 * Describes a user in any authenticated session. Implementations can provide their own validation
 * processing. There are two implementations: one for the server (which has life-or-death rights
 * over user records) and one for the client (where users can only be defined through certificates
 * or be anonymous).
 * 
 * @author Ferd
 *
 */
public interface IUser {

    /**
     * points to the public key filename in the global Configuration properties.
     */
    public static final String THINKLAB_PUBLIC_KEY_PROPERTY = "thinklab.public.key";

    /**
     * ID for an anonymous user. Unsurprising.
     */
    public static final String ANONYMOUS_USER_ID            = "anonymous";

    /**
     * Keys for user properties in certificates or for set operations.
     */
    public static final String REALNAME                     = "realname";
    public static final String FIRSTNAME                    = "firstname";
    public static final String EMAIL                        = "email";
    public static final String LASTNAME                     = "lastname";
    public static final String INITIALS                     = "initials";
    public static final String PHONE                        = "phone";
    public static final String ADDRESS                      = "address";
    public static final String AFFILIATION                  = "affiliation";
    public static final String JOBTITLE                     = "jobtitle";
    public static final String COMMENTS                     = "comments";
    public static final String SKEY                         = "key";
    public static final String USER                         = "user";
    public static final String ROLES                        = "roles";
    public static final String GROUPS                       = "groups";
    public static final String MODULES                      = "modules";
    public static final String EXPIRY                       = "expiry";
    public static final String SERVER                       = "primary.server";

    public static final Object ROLE_ADMINISTRATOR           = "ADMIN";

    /**
     * Never null, may be ANONYMOUS_USER_ID when isAnonymous() returns true.
     * 
     * @return
     */
    String getUsername();

    /**
     * List roles for this user. So far, only role of relevance is ADMIN - roles are just strings,
     * stored at server side and defined at authenticate(). Roles do not imply assets but are checked
     * for privileged operations.
     * 
     * @return
     */
    Set<String> getRoles();

    /**
     * List groups the user belongs to. At client side, only non-empty after authenticate() is
     * successful.
     * 
     * @return
     */
    Set<String> getGroups();

    /**
     * For future use: list plug-ins that the user has a right to at server side. 
     * 
     * @return
     */
    Set<String> getModules();

    /**
     * True after the empty constructor is used. In this situation, isOnline() is false, the
     * assets are empty (not null) and the username is "anonymous". 
     * 
     * @return
     */
    boolean isAnonymous();

    /**
     * Assets are harvested from the group set and synchronized at authentication with the
     * server. If that fails (!isOnline()), the assets may not reflect what the user has
     * been assigned.
     * 
     * @return
     */
    IUserAssets getAssets();

    /**
     * Primary server URL, harvested from the group set at the server side and stored in the
     * certificate. One of three bits of data that the certificate contains along with username
     * and email (plus optionally basic anagraphic data for pretty-printing at client side). This
     * one can be null (in anonymous and unprivileged users). If null, everything works but the
     * whole system is essentially a self-contained sandbox.
     * 
     * @return
     */
    String getServerURL();

    /**
     * A UUID generated once and for all at server side, stored in the certificate and used for
     * authentication in every client/server request. Has nothing to do with the user's online
     * password, which is only relevant for cloud services web access.
     * 
     * @return
     */
    String getSecurityKey();

    /**
     * Never null - users cannot be created at server side without an email address.
     * 
     * @return
     */
    String getEmailAddress();

    /**
     * May be null if user has been created in non-standard ways.
     *  
     * @return
     */
    String getFirstName();

    /**
     * May be null if user has been created in non-standard ways.
     * 
     * @return
     */
    String getLastName();

    /**
     * OK, Anglo-saxons, have it your way. At least it can be null.
     * 
     * @return
     */
    String getInitials();

    /**
     * Input by user at registration, possibly null.
     * 
     * @return
     */
    String getAffiliation();

    /**
     * Strings from a controlled vocabulary to assess options checked or unchecked at registration, such as
     * "send updates". Unused at the moment.
     * 
     * @return
     */
    Set<String> getOptions();

    /**
     * Return whatever further comments were entered by user at registration.
     * 
     * @return
     */
    String getComment();

    // TODO only leave in implementation
    void set(String key, String value);

    /**
     * Read the passed certificate and set the user to it. If certificate is invalid or expired, return
     * false and set the user to anonymous. Should be passed an existing file. The public key file must be 
     * a valid file name in the global configuration with property THINKLAB_PUBLIC_KEY_PROPERTY; 
     * if not, this operation should not be attempted. 
     * 
     * Only used at client side.
     * 
     * @param license
     * @return
     */
    boolean authenticate(File certificate);

    // TODO remove and leave in the implementation only
    void setLastLogin(long time);

    /**
     * Date of last login for user. Should be kept up to date at server side when authorizing, and
     * correspond to authentication time at client side.
     * 
     * @return
     */
    Date getLastLogin();

    /**
     * True if the user is successfully certified (has a valid certificate) at the client side, but the
     * authentication with the primary server didn't work for any reason. At the server side, this should
     * always return true.
     * 
     * @return
     */
    boolean isOnline();

    /**
     * generate a random password, set it on the object (without saving to the database), and return
     * the newly generated password.
     * 
     * @return
     */
    String setRandomPassword();

}
