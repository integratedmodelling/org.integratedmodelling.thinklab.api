package org.integratedmodelling.thinklab.api.runtime;

import java.io.File;
import java.util.List;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.ICoverage;
import org.integratedmodelling.thinklab.api.modelling.IObservation;
import org.integratedmodelling.thinklab.api.modelling.ISubject;

/**
 * 
 * @author ferdinando.villa
 * 
 */
public interface IContext {

    /**
     * Each context has an ID that's unique within a session.
     * 
     * @return
     */
    long getId();

    /**
     * True if the context has been created without any concrete observation in it. It may contain abstracts
     * (space, time etc) or not at this stage, but no root object.
     * 
     * @return
     */
    boolean isEmpty();

    /**
     * Contexts take their name from the root subject they observe. The name does not need to be unique and
     * will appear in visualizations, so it should be user-acceptable and informative.
     * 
     * @return
     */
    String getName();

    /**
     * True if context is not empty and all states have been fully contextualized; this means that either
     * there is no time scale, or if so, run() was called to complete observation in time and has finished. It
     * will also return true if the schedule was interrupted from the monitor or by internal error conditions.
     * 
     * Monitor functions should be used to understand whether the execution had errors or was interrupted
     * before accessing states and objects.
     * 
     * @return
     */
    boolean isFinished();

    /**
     * True if run() has been called and isFinished() returns false.
     * 
     * @return
     */
    boolean isRunning();

    /**
     * Return a proxy to this context that will make all the observations called on it affected by the passed
     * scenarios. It will reset any scenarios previously defined.
     * 
     * @param scenarios
     * @return a new IContext that wraps the original object, but whose model choices will be affected by the
     * scenarios. The states created will be in the same subject.
     */
    IContext inScenario(String... scenarios);

    /**
     * If context is empty, set it to passed observable (which must be a ISubjectGenerator or an endurant
     * concept); else observe the passed observable in this context. Waits for the task to end and returns the
     * context after it. Use observeAsynchronous to control the task.
     * 
     * Returns null if the context is running or past the first step().
     * 
     * @param observable
     * @return self to enable fluent idioms. Implementations may be as functional as they want here, but a
     * IContext is all about building state incrementally, so no need to make it return a new object.
     */
    IContext observe(Object... observable);

    /**
     * If context is empty, set it to passed observable (which must be a ISubjectGenerator or an endurant
     * concept); else observe the passed observable in this context. Return the task associated with the
     * observation, which has been started and may be finished or ongoing.
     * 
     * @param observable
     * @return self to enable fluent idioms. Implementations may be as functional as they want here, but a
     * IContext is all about building state incrementally, so no need to make it return a new object.
     */
    ITask observeAsynchronous(ITask.Listener listener, Object... observable);

    /**
     * Start contextualization by creating the temporal observation strategy and running it. This may do
     * nothing if there is no time scale or contextualization has already happened.
     * 
     * It should operate asynchronously and return without blocking. It is possible to observe() things while
     * contextualization is ongoing. The time scale may be infinite (real time) - in such cases, the monitor
     * is used to interrupt explicitly.
     * 
     * It is an error to call run() on anything other than the root context for an observation session. If a
     * dependent subject exists when run() is called, that is run as well; if a new subject is observed within
     * this context and contextualization is running, it should be initialized, inserted and run immediately.
     * @throws ThinklabException
     */
    ITask run();

    /**
     * Set client time to index of passed transition of the main subject. May or may not produce the intended
     * results - in real time contexts with multi-threaded independent agents, it may just produce runtime
     * errors or unreliable results. Provided for debugging of simple cases.
     */
    ITask setTime(int timeStep);

    /**
     * Get the coverage after initialization or after of the last operation on the context.
     * 
     * @return
     */
    ICoverage getCoverage();

    /**
     * The root subject.
     * 
     * @return
     */
    ISubject getSubject();

    /**
     * Return all the tasks that have applied to this context in order of execution.
     * 
     * @return
     */
    List<ITask> getTasks();

    /**
     * Return the subject or state identified by the passed path. Paths are returned by {@link
     * IObservation.getPath()}. Used to identify observations in client/server communication.
     * 
     * @param path
     * @return
     */
    IObservation get(String path);

    /**
     * Persist to passed file (may be a directory, according to implementation). A new read-only IContext
     * should be able to be reconstructed from the resulting file.
     * 
     * @param file
     * @param path persist the passed observation or the whole context if path is null or "/".
     */
    void persist(File file, String path) throws ThinklabException;

    /**
     * If true is passes, return a context that will reset the root subject to a pristine observation before
     * making the next one. Otherwise just return self.
     * 
     * @param reset
     * @return
     */
    IContext resetContext(boolean reset);
}
