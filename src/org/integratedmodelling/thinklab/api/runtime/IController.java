package org.integratedmodelling.thinklab.api.runtime;

import java.util.List;

/**
 * The interface for a Thinklab controller in a MVC sense. The common package provides a
 * tiny REST server that can be bound to a IController, so that the actions of a user in an
 * independent UI interface (like the future GWT runtime or the embedded spatial editor
 * in ThinkCap) can be handled.
 * 
 * @author Ferd
 *
 */
public interface IController {

    public static final int DEFAULT_PORT = 8109;

    /**
     * Process command and respond to visualization. Respond null if no response is required.
     * 
     * @param command
     * @param arguments
     * @return
     */
    public String commandReceived(String command, List<String> arguments);

}
