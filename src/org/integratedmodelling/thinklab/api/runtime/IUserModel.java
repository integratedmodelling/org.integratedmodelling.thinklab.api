package org.integratedmodelling.thinklab.api.runtime;

import java.io.InputStream;
import java.io.PrintStream;

/**
 * Implements the model of the user during a Thinklab session. Only one user model should exist per session, and it can be retrieved from it; 
 * it may also be null for non-interactive sessions. Application tasks and interface components will check the lineage of the user model 
 * in order to enable or disable operations.
 * 
 * FIXME update to use IUser and use roles for administration etc. Must link in auth libraries.
 * 
 * @author  Ferdinando
 */
public interface IUserModel {

    /**
     * Sessions may have an input and an output stream associated in case they can interact with the user through them.
     * @return
     * @uml.property  name="inputStream"
     */
    InputStream getInputStream();

    /**
     * Sessions may have an input and an output stream associated in case they can interact with the user through them.
     * @return
     * @uml.property  name="outputStream"
     */
    PrintStream getOutputStream();

    /*
     * initialize, passing the session that this user works in
     */
    void initialize(ISession session);

    /**
     * return whatever user name was authenticated, or IUser.ANONYMOUS_USER_ID if none was.
     * FIXME return a proper IUser.
     */
    IUser getUser();
}
