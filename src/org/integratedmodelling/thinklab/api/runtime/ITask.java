package org.integratedmodelling.thinklab.api.runtime;

import java.util.Date;
import java.util.List;

import org.integratedmodelling.thinklab.api.listeners.INotification;

/**
 * A ITask is the process corresponding to an atomic observation in a context.
 * It is returned by {@link IContext.observe()} immediately; it will return 
 * a result when finished. If a synchronous behavior is desired, finish() will
 * return when the computation is finished, and return the context so that
 * other observations can be called on it (context.observe().finish().observe())
 * 
 * A task may create its own context. This is because we may not know the observable
 * the moment the task is created: all tasks should update the model space before 
 * resolving the observable. So the first task will have no context associated until
 * finished.
 *  
 * @author ferdinando.villa
 *
 */
public interface ITask {

    public static enum Status {
        WAITING,
        RUNNING,
        INITIALIZING,
        FINISHED,
        ERROR,
        INTERRUPTED
    }

    /**
     * Use a listener to monitor the task's lifetime. Passed to IContext.observeAsynchronous().
     * 
     * @author Ferd
     *
     */
    public static interface Listener {

        void started(ITask task);

        void finished(ITask task);

        void error(ITask task, Throwable throwable);

        void interrupted(ITask task);

        void notification(ITask task, INotification notification);

        void contextAvailable(IContext context);

        void timeTransition(IContext context, long start, long end);
    }

    /**
     * Peek current status and return it.
     * @return
     */
    Status getStatus();

    /**
     * Task have an ID that's unique within a session. getId() would be a better
     * name, but at the server side we want tasks to be threads, and those have
     * a conflicting getId() that we don't want to mess with.
     * 
     * @return
     */
    long getTaskId();

    /**
     * The command that has started the task. May be null.
     * 
     * @return
     */
    String getCommand();

    /**
     * Task description. May be null.
     * 
     * @return
     */
    String getDescription();

    /**
     * Interrupt task at the first chance. Task cannot be resumed
     * after this. Use the listener to check for actual interruption.
     * 
     */
    void interrupt();

    /**
     * Return the context this task belongs to. Will return null until the context is
     * set; the Listener can be used to catch the context as it's created.
     * 
     * @return
     */
    IContext getContext();

    /**
     * Wait until end of task and return the context after the
     * task has completed.
     * 
     * @return
     */
    IContext finish();

    /**
     * Retrieve all notifications from completion of the task. 
     * 
     * @return
     */
    List<INotification> getNotifications();

    /**
     * Return the start time. This is never null - if the task is waiting, return when we started waiting.
     * 
     * @return
     */
    Date getStartTime();

    /**
     * Return the end time. This is only not null if the task status is finished.
     * 
     * @return
     */
    Date getEndTime();

}
