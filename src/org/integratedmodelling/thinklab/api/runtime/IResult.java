package org.integratedmodelling.thinklab.api.runtime;

public interface IResult {

    public int getStatus();

    public String getCommand();

    public String getOutput();

    public Throwable getException();

    public Object getResult();

    public long getStartTime();

    public long getEndTime();

}
