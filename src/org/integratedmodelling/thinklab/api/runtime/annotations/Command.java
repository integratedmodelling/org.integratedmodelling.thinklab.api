package org.integratedmodelling.thinklab.api.runtime.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * All arguments have names that act as keywords that are local to the 
 * command. They may be left unspecified and are matched
 * to all mandatory arguments in order of declaration. Optional arguments
 * names are only matched when introduced by -<short name> or --<long name>.
 * 
 * Arguments are string that may include additional specifications:
 * 
 * - single string = mandatory argument with string type. If string contains
 *   two arguments separated by | they're interpreted as short and full name, e.g.
 *   
 *   	"? o|output"  // short names only useful with options
 * 
 * - optional flag is a ? as a separate token: "? format"
 * 
 * - argument type[s] can be given at the end; > 1 argument type = multiple
 * 	 arguments; option without arguments is specified by giving owl:Nothing type
 *   and assumed to be optional. No type means one argument of thinklab-core:Text
 *   type.
 *   
 *   	"output thinklab-core:Text"
 *   
 *   Instead of the type, a set of tokens separated by | can identify 
 *   the admitted options:
 *   
 *   	"? format gif|png|jpg"
 *   
 * @author ferdinando.villa
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Command {
    public String id();

    public String[] arguments();

    public String returnType() default "owl:Nothing";

    String description() default "";
}
