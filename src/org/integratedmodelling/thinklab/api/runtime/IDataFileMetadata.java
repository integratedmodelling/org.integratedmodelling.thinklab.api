package org.integratedmodelling.thinklab.api.runtime;

public interface IDataFileMetadata {
    public static final String ASPATIAL_SCOPE = "ASPATIAL";
    public static final String GLOBAL_SCOPE = "GLOBAL";

    public static final String UNIQUE_ID = "uniqueId";
    public static final String FILE_LIST = "fileList";

    public static final String NAMESPACE = "namespace";
    public static final String REGION = "region";
    public static final String COUNTRY = "country";
    public static final String CONTINENT = "continent";
    public static final String FILESET_NAME = "filesetName";
    public static final String FILESET_TYPE = "filesetType";
}
