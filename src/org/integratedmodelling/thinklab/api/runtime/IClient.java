//package org.integratedmodelling.thinklab.api.runtime;
//
//import java.util.Properties;
//import java.util.logging.Level;
//
//import org.integratedmodelling.exceptions.ThinklabException;
//import org.integratedmodelling.thinklab.api.runtime.IServer.Response;
//
///**
// * A Client is intended as a user session - either GUI, command line or both. It keeps one IServer
// * internally and can switch the IServer to another, transparently handling user sessions. The Client
// * is initialized using a Properties object (e.g. decrypted from a license file) that contains all
// * user data, roles and privileges. 
// * 
// * The client distinguishes commands from language statements; all language statements belong to
// * a user namespace that is ephemeral. A command history (remembering both commands and statements)
// * is supported in the API.
// * 
// * @author Ferd
// * @deprecated either use IServer or rename IServer to IClient
// */
//public interface IClient {
//
//    /**
//     * Callback to pass to each action function. Each action is given a task ID which is passed
//     * to the start callback. The end callback will contain the result of the operation.
//     * 
//     * @author Ferd
//     *
//     */
//    public interface TaskCallback {
//
//        void taskStarted(ITask task);
//
//        void taskFinished(ITask task, Response status);
//
//        void taskInterrupted(ITask task);
//
//        void log(ITask task, Object log, Level level);
//    }
//
//    /**
//     * Callback for the server operations. Only passed to boot(), remains alive for all the
//     * lifetime of the client.
//     * 
//     * @author Ferd
//     *
//     */
//    public interface ServerCallback {
//
//        void serverStarting(IServer server);
//
//        void serverStarted(IServer server);
//
//        void serverStopped(IServer server);
//
//        void log(Object log, Level level);
//    }
//
//    /**
//     * Available to call in an if statement whenever an operation that requires server
//     * availability is called. Should return false when the server is BUSY and should not
//     * be called if the server is DISCONNECTED or null. It is meant to be called interactively
//     * when an the operation is requested by the user, so it should beep or produce a warning
//     * before returning false. 
//     * 
//     * @return
//     */
//    boolean isServerAvailable();
//
//    /**
//     * Initialization and (possibly) startup of the default/configured server. The passed
//     * properties should identify all we need to know about the user.
//     * 
//     * @param properties
//     * @throws ThinklabException
//     */
//    void boot(Properties properties, ServerCallback callback) throws ThinklabException;
//
//    /**
//     * Shutdown is reported to 
//     */
//    void shutdown();
//
//    /**
//     * Send a command to the server. Return the task ID.
//     * @param command
//     * @param callback
//     * @throws ThinklabException
//     */
//    int send(String command, TaskCallback callback) throws ThinklabException;
//
//    /**
//     * Return the current server, or null if no current server is defined. The 
//     * server may or may not be running.
//     * 
//     * @return
//     */
//    IServer getServer();
//
// }
