package org.integratedmodelling.thinklab.api.runtime;

import java.net.URL;

/**
 * Small data structure describing a server and the operations an user can do on
 * it. Created on a user-by-user base; will return different data for different 
 * users, reflecting different levels of access. These data are normally taken
 * from the user's certificate, with some checked at runtime at authentication.
 * 
 * @author Ferd
 *
 */
public interface IServerDescription {

    /**
     * the URL of the server.
     * 
     * @return
     */
    public URL getURL();

    /**
     * true if this user can lock the server for development and upgrade.
     * 
     * @return
     */
    public boolean canLock();

    /**
     * true if this user can use the server for resolving observations, and
     * there are observations to be resolved in it.
     * 
     * @return
     */
    public boolean canSearch();

    /**
     * Return an integer from 0 to 5 to describe the computing capability of the 
     * server. Higher-privileged users will get the most capable server available
     * by default.
     * 
     * @return
     */
    public int getServerClass();

}
