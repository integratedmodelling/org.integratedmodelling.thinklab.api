/**
 * ISession.java
 * ----------------------------------------------------------------------------------
 * 
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of Thinklab.
 * 
 * Thinklab is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thinklab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * ----------------------------------------------------------------------------------
 * 
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @author    Ioannis N. Athanasiadis (ioannis@athanasiadis.info)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.thinklab.api.runtime;

import java.util.List;
import java.util.Properties;

import org.integratedmodelling.thinklab.api.listeners.IListenable;
import org.integratedmodelling.thinklab.api.listeners.INotification;
import org.integratedmodelling.thinklab.api.modelling.INamespace;

/**
 * A Session is a temporary concept space that contains all instances that are created during a user session. A session should be an ontology or contain one, but its identity as a IOntology is not mandated. Sessions are normally memory-based but should be able to persist themselves. All operations on the same session are synchronous so there's no need to worry about concurrency.
 * @author  Ferdinando Villa
 */
public interface ISession extends IListenable {

    /**
     * Each session has a unique ID 
     */
    String getID();

    /**
     * A session must have properties that users and plugins can use to send data
     * around. This method must return a valid properties object. Configuration is
     * optional - obvious things like the user data should be in the user model, not
     * in the session.
     * 
     * @return  the session's properties.
     */
    Properties getProperties();

    /**
     * Return the user model for the session. If the session is not interactive, the user model may be null.
     * The user model determines the input and output streams connected with the session, which
     * may also be null.
     * 
     * @return
     */
    IUserModel getUserModel();

    /**
     * Print a string wherever is appropriate, or do nothing if not appropriate. Do not
     * raise errors.
     * 
     * @param s
     */
    void print(String s);

    /**
     * Print a string wherever is appropriate, or do nothing if not appropriate. Do not
     * raise errors.

     * @param s
     */
    void println(String s);

    /**
     * Return a unique directory name for the session's workspace, in case applications request it. 
     * The directory  should be a simple identifier to be used to create a workspace wherever the
     * application wants to put it.
     * 
     * @return
     */
    String getWorkspace();

    /**
     * Register a notification. Used internally and by any IMonitor object passed to task operations.
     * 
     * @param notification
     */
    void notify(INotification notification);

    /**
     * Return all notifications available for this session. Clear the notifications if true is 
     * passed, so that they won't be reported at the next call.
     * 
     * @param clear
     * @return
     */
    List<INotification> getNotifications(boolean clear);

    /**
     * If operations done in a session produce knowledge, a namespace may be
     * necessary. This should only create a namespace when requested, and 
     * return the same namespace in subsequent calls.
     * @return
     */
    INamespace getNamespace();

    /**
     * Observe the passed object, which must be able to generate a subject (either a ISubjectGenerator, an
     * ID for a language object that can specify it, or other implementation-dependent observables). 
     * Immediately return a running task that will generate the observation and push the corresponding 
     * context to the session's internal stack when it becomes available. A listener can be passed to 
     * intercept the lifetime of the task and the creation of the context.
     * 
     * Return null without error if the observation cannot be made. If a task is generated, all 
     * exceptions raised in the observation should be reported to the listener and not thrown.
     * 
     * @param subject
     * @param listener
     * @return
     */
    ITask observe(Object subjectGenerator, ITask.Listener listener);

    /**
     * Return the most current context created. If no observation were made or the last context has
     * expired, return null without error.
     * 
     * @return
     */
    IContext getTopContext();

    /**
     * Return all the contexts in the history of the session in order of creation (i.e., the "current" 
     * context will be last). These may be less than the ones actually computed if the history size is 
     * limited by the implementation.
     * 
     * @return
     */
    List<IContext> getContexts();

    /**
     * Return the context with the specified ID. It may return null even for a valid ID if the context has
     * expired.
     * 
     * @return
     */
    IContext getContext(long contextId);

    /**
     * Return the list of all tasks that we have executed.
     * 
     * @return
     */
    List<ITask> getTasks();
}
