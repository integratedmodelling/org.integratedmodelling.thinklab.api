package org.integratedmodelling.thinklab.api.runtime;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;

public interface ICommandHandler {

    /**
     * 
     * @param parameters
     * @param options
     * @param input
     * @param output
     * @return
     */
    Object execute(HashMap<String, Object> parameters, HashMap<String, Object> options, InputStream input,
            PrintWriter output);

}
