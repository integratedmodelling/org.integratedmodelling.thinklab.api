package org.integratedmodelling.thinklab.api.runtime;

import java.util.Collection;
import java.util.List;
import java.util.logging.Level;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.factories.IKnowledgeManager;
import org.integratedmodelling.thinklab.api.lang.IMetadataHolder;
import org.integratedmodelling.thinklab.api.lang.IPrototype;
import org.integratedmodelling.thinklab.api.project.IProject;

/**
 * Represents a Thinklab server that we can use. Remote or local
 * should not make any difference. Objects returned by the main
 * methods must implement the appropriate interfaces, all of which
 * are immutable.
 * 
 * Its methods should not throw exceptions; error retrieval methods
 * should be used to check status.
 * 
 * Should contain a session and represent both multi-user with authentication 
 * and single user, embedded servers.
 * 
 * @author Ferd
 *
 */
public interface IServer extends IMetadataHolder {

    /*
     * status codes for results and task management
     */
    public static final int    OK                     = 0;
    public static final int    ERROR                  = -1;
    public static final int    SCHEDULED              = -2;
    public static final int    RUNNING                = -3;

    /*
     * metadata fields that you should fill out as applicable if you're nice,
     * but not count on to exist.
     */
    public static final String CURRENT_USERNAME       = "IServer.CURRENT_USERNAME";
    public static final String BOOT_TIME_MS           = "IServer.BOOT_TIME_MS";
    public static final String TOTAL_MEMORY_MB        = "IServer.TOTAL_MEMORY_MB";
    public static final String FREE_MEMORY_MB         = "IServer.FREE_MEMORY_MB";
    public static final String LOAD_FACTOR_PERCENT    = "IServer.LOAD_FACTOR_PERCENT";
    public static final String VERSION_STRING         = "IServer.VERSION_STRING";
    public static final String AVAILABLE_PROCESSORS   = "IServer.AVAILABLE_PROCESSORS";
    public static final String EXCEPTION_CLASS        = "IServer.EXCEPTION_CLASS";
    public static final String STACK_TRACE            = "IServer.STACK_TRACE";
    public static final String UPTIME_TEXT            = "IServer.UPTIME_TEXT";
    public static final String IS_LOCKED              = "IServer.IS_LOCKED";
    /**
     * Servers use this workspace subarea as an exchange for ontologies; clients
     * read knowledge from there after server was started.
     */
    public static final String KNOWLEDGE_STORAGE_AREA = "knowledge";
    
    /**
     * Environmental variable that may be substituted in the script that launches an
     * embedded server.
     */
    public static final String ENV_THINKLAB_MAX_MEMORY = "THINKLAB_MAX_MEMORY";
    public static final String ENV_THINKLAB_JRE = "THINKLAB_JRE";

    /**
     * This is used to communicate results of operations. Simplifies access to 
     * maps and arrays, under the assumption that each command's API is known.
     *
     * @author Ferd
     *
     */
    public static interface Response {

        public int getStatus();

        public String getCommand();

        public String getOutput();

        public Throwable getException();

        public Object getResult();

        public Object get(int n);

        public Object get(String key);
    }

    /**
     * Callback for the server operations. Only passed to boot(), remains alive for all the
     * lifetime of the client.
     * 
     * @author Ferd
     *
     */
    public interface ServerCallback {

        void serverStarting(IServer server);

        void serverStarted(IServer server);

        void serverStopped(IServer server);

        void log(Object log, Level level);
    }

    /**
     * Execute a statement in modeling language. Block until
     * result is returned.
     * 
     * @param s
     * @param taskId user-defined task ID to monitor the execution through the notification system.
     * @return
     */
    Response executeStatement(String s, int taskId);

    /**
     * Execute a command line instruction. Block until result
     * is returned.
     * 
     * @param command
     * @return
     */
    Response executeCommand(String command, Object... args);

    /**
     * Start the thing, after which it must either report error status or be
     * ready to accept commands. The passed listener gets all notifications that
     * are not directed to specific tasks.
     */
    Response boot();

    /**
     * Always call this when done.
     * 
     */
    Response shutdown();

    /**
     * True if server is active, connected and authenticated, ready to do stuff.
     * 
     * @return
     */
    boolean isActive();

    /**
     * Deploy project (and all its prerequisites). Deploy means ensuring the project is current and
     * registered, not loaded, at the server side. If privileges are not enough or there are errors,
     * just return false.
     * 
     * @param p
     * @return
     * @throws ThinklabException 
     */
    Response deploy(IProject p) throws ThinklabException;

    /**
     * Undeploy project. Behavior as expected for deploy().
     * 
     * @param p
     * @return
     */
    Response undeploy(String projectId);

    /**
     * Return prototypes for all known functions.
     * @return
     */
    List<IPrototype> getFunctionPrototypes();

    /**
     * Return prototypes for all known commands.
     * @return
     */
    List<IPrototype> getCommandPrototypes();

    /**
     * Return the current client session, or null if not connected.
     * 
     * @return
     */
    ISession getCurrentSession();

    /**
     * Import and load all the Thinklab projects registered, handling their dependencies appropriately.
     * 
     * @param projectDirectory
     * @return 
     */
    Response loadAll(Collection<IProject> projects);

    /**
     * A server should publish its knowledge manager to provide knowledge resolution and
     * reasoning services. The KM resulting from this one should know the same things
     * the remote server knows, although it should not be expected to provide annotation
     * services.
     * 
     * Never return null - if need be, return a dumb KM that does nothing.
     * 
     * @return
     */
    IKnowledgeManager getKnowledgeManager();

    /**
     * Send command directly without parsing a command line. This works like Session.send() and 
     * blocks until command returns. First argument is the command, other arguments should be in
     * pairs key, value unless they reference options with no arguments.
     * 
     * @param cmd
     * @return
     */
    Response send(String... cmd);

    /**
     * Get the URL for the passed command.
     * @param string
     * @return
     */
    String getCommandURL(String command, Object... arguments);

}
