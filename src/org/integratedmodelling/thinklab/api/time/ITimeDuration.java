package org.integratedmodelling.thinklab.api.time;

import org.integratedmodelling.collections.Pair;

public interface ITimeDuration extends Comparable<ITimeDuration> {

    /**
     * number of milliseconds in this duration. It might be a bit too implementation-specific, but for now I'm
     * leaving it as-is because it's a good interface to describe the one implementation that currently exists
     * (DurationValue)
     *
     * @return
     */
    long getMilliseconds();

    /**
     * Localize a duration to an extent starting at the current moment
     * using the same resolution that was implied in the generating
     * text. For example, if the duration was one year, localize to the
     * current year (jan 1st to dec 31st). Return the start and end points
     * of the extent.
     *
     * @return
     */
    Pair<ITimeInstant, ITimeInstant> localize();
}
