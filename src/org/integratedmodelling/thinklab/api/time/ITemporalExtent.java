package org.integratedmodelling.thinklab.api.time;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.IExtent;

public interface ITemporalExtent extends IExtent {
    /**
     * Overriding to constrain the result type further (from IExtent to ITemporalExtent)
     */
    @Override
    ITemporalExtent getExtent(int stateIndex);

    /**
     * Overriding to require that the collapsed type is ITimePeriod. This allows simpler coding against the API,
     * and is the most logical way to enforce that getValueCount() == 1.
     */
    @Override
    ITimePeriod collapse();

    /**
     * Overriding to constrain the result type further
     */
    @Override
    public ITemporalExtent intersection(IExtent other) throws ThinklabException;

    ITimeInstant getStart();

    ITimeInstant getEnd();
}
