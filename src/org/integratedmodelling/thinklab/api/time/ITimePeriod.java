package org.integratedmodelling.thinklab.api.time;

import org.integratedmodelling.exceptions.ThinklabException;
import org.integratedmodelling.thinklab.api.modelling.IExtent;

/**
 * Authoritative type for expressing an anchored (specific start/end) duration of time.
 *
 * The semantics for observation over time dictates that states are OBSERVED at (i.e. read from) a given time,
 * whereas states are GENERATED for (i.e. written to) the instant immediately following the given time.
 * Put more plainly, one READS from the present, and WRITES to the future.
 *
 * @author luke
 *
 */
public interface ITimePeriod extends ITemporalExtent {

    /**
     * whether or not the time period contains the given instant, using exclusive-start, inclusive-end
     * semantics.
     *
     * @param time
     * @return
     */
    public boolean contains(ITimeInstant time);

    /**
     * whether or not the time period contains the given instant (ms since Jan 1, 1970), using
     * exclusive-start, inclusive-end semantics.
     *
     * @param millisInstant
     * @return
     */
    public boolean contains(long millisInstant);

    /**
     * whether or not the time period ends before the instant, using exclusive-start, inclusive-end semantics.
     *
     * @param instant
     * @return
     */
    public boolean endsBefore(ITimeInstant instant);

    /**
     * whether or not the time period ends before the start instant of the other period, using
     * exclusive-start, inclusive-end semantics.
     *
     * @param other
     * @return
     */
    public boolean endsBefore(ITemporalExtent other);

    /**
     * whether or not the two time periods overlap, using exclusive-start, inclusive-end semantics.
     *
     * @param other
     * @return
     */
    public boolean overlaps(ITemporalExtent timePeriod);

    /**
     * Overriding to constrain return type
     */
    @Override
    public ITemporalExtent union(IExtent other) throws ThinklabException;

}
