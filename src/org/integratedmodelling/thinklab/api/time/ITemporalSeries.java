package org.integratedmodelling.thinklab.api.time;

import java.util.Collection;
import org.integratedmodelling.exceptions.ThinklabException;

public interface ITemporalSeries<T> extends ITemporalExtent {
    /**
     * return the item which is valid for the given time, or null if none qualifies.
     */
    public T getAtTime(ITimeInstant time);

    /**
     * get the earliest temporal scale item with a start time AFTER or EQUAL TO the given time
     */
    public T getFollowing(ITimeInstant time);

    /**
     * get the latest temporal scale item with an end time BEFORE the given time
     */
    public T getPrior(ITimeInstant time);

    public T getFirst();

    public T getLast();

    /**
     * return a collection of the items which are valid at any time during timePeriod, or an empty collection
     * if none qualifies.
     *
     * @param timePeriod
     * @return
     */
    public Collection<T> getOverlapping(ITimePeriod timePeriod);

    public void put(ITimeInstant start, ITimeInstant end, T item) throws ThinklabException;

    public void put(ITimePeriod timePeriod, T item);

    /**
     * shorten the time period of the item which contains the splice time
     *
     * @param spliceTime
     * @return
     * @throws ThinklabException
     */
    public ITimePeriod shorten(ITimeInstant spliceTime) throws ThinklabException;

    public void remove(ITimeInstant time);

    public ITimePeriod bisect(ITimeInstant spliceTime, T object) throws ThinklabException;
}
