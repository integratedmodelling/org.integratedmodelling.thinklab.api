package org.integratedmodelling.thinklab.api.time;

public interface ITimeInstant extends Comparable<ITimeInstant> {

    long getMillis();

}
