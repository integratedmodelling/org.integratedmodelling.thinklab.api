package org.integratedmodelling.thinklab.api.connection;

import java.io.File;

import org.integratedmodelling.exceptions.ThinklabException;

/**
 * Connection to a collection of ISources, each of which is translatable to
 * a function that imports the source itself. Can be a folder, a service or
 * a dataset.
 * 
 * @author Ferd
 *
 */
public interface IConnection extends Iterable<ISource> {

    /**
     * Import the given source if possible. Return false if not imported, only
     * throw an exception in case of runtime error for importable ones.
     * 
     * @param source
     * @return
     * @throws ThinklabException
     */
    public boolean importSource(ISource source) throws ThinklabException;

    /**
     * Write to an appropriately named connection file that can be read by
     * ConnectionFactory, located in the passed directory.
     * 
     * @param folder
     * @return
     * @throws ThinklabException
     */
    public File write(File folder) throws ThinklabException;

    /**
     * Return true if there were errors; in that case, write() will throw an exception.
     * 
     * @return
     */
    public boolean hasErrors();

    /**
     * Number of sources in the connection. 
     * 
     * @return
     */
    public int getSourcesCount();

    /**
     * Each connection must have a display name without spaces.
     * @return
     */
    public String getName();

    /**
     * Return the filename this would be written to.
     * 
     * @return
     */
    public String getFileName();

}
