package org.integratedmodelling.exceptions;

import org.integratedmodelling.exceptions.ThinklabException;

public class ThinklabInvalidObservationException extends ThinklabException {
    private static final long serialVersionUID = 1L;

    public ThinklabInvalidObservationException() {
        super(
                "An observation was requested on a subject which was not capable of providing the observation. Usually this means that the subject is dead.");
    }
}
