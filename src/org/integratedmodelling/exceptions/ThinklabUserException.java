package org.integratedmodelling.exceptions;

public class ThinklabUserException extends RuntimeException {

    private static final long serialVersionUID = 5210321756938931423L;

    public ThinklabUserException() {
    }

    public ThinklabUserException(String message) {
        super(message);
    }

    public ThinklabUserException(Throwable cause) {
        super(cause);
    }

    public ThinklabUserException(String message, Throwable cause) {
        super(message, cause);
    }

}
