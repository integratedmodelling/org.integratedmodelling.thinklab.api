/**
 * Triple.java
 * ----------------------------------------------------------------------------------
 * 
 * Copyright (C) 2008 www.integratedmodelling.org
 * Created: Jan 17, 2008
 *
 * ----------------------------------------------------------------------------------
 * This file is part of Thinklab.
 * 
 * Thinklab is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Thinklab is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with the software; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 * 
 * ----------------------------------------------------------------------------------
 * 
 * @copyright 2008 www.integratedmodelling.org
 * @author    Ferdinando Villa (fvilla@uvm.edu)
 * @author    Ioannis N. Athanasiadis (ioannis@athanasiadis.info)
 * @date      Jan 17, 2008
 * @license   http://www.gnu.org/licenses/gpl.txt GNU General Public License v3
 * @link      http://www.integratedmodelling.org
 **/
package org.integratedmodelling.collections;

import java.io.Serializable;

import org.integratedmodelling.common.IStoredByValue;

/**
  * Stupid generic pair class.
 */
public class Triple<T1, T2, T3> implements Serializable, IStoredByValue {
    private static final long serialVersionUID = 8017164648757388295L;
    protected T1 first = null;
    protected T2 second = null;
    protected T3 third = null;

    /**
     *  Pair constructor comment.
     */
    public Triple() {
    }

    /**
     * @param  first java.lang.Object
     * @param  second java.lang.Object
     */
    public Triple(T1 first, T2 second, T3 third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public Triple(Triple<T1, T2, T3> tc) {
        this.first = tc.first;
        this.second = tc.second;
        this.third = tc.third;
    }

    /**
     * @param  newValue java.lang.Object
     */
    public void setFirst(T1 newValue) {
        this.first = newValue;
    }

    /**
     * @param  newValue java.lang.Object
     */
    public void setSecond(T2 newValue) {
        this.second = newValue;
    }

    /**
     */
    public void setThird(T3 newValue) {
        this.third = newValue;
    }

    /**
     * @return  java.lang.Object
     */
    public T1 getFirst() {
        return first;
    }

    /**
     * @return  java.lang.Object
     */
    public T2 getSecond() {
        return second;
    }

    public T3 getThird() {
        return third;
    }

    /**
     * @return  java.lang.String
     */
    public String toString() {
        return "{" + getFirst() + "," + getSecond() + "," + getThird() + "}";
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof Triple))
            return false;

        return Pair.cmpObj(first, ((Triple<?, ?, ?>) obj).first)
                && Pair.cmpObj(first, ((Triple<?, ?, ?>) obj).second)
                && Pair.cmpObj(third, ((Triple<?, ?, ?>) obj).third);
    }

    @Override
    public int hashCode() {
        return (first == null ? 0 : first.hashCode()) + (second == null ? 0 : second.hashCode())
                + (third == null ? 0 : third.hashCode());
    }

}
