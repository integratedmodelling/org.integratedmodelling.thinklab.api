package org.integratedmodelling.lang;

/**
 * May be implemented by objects returned by commands. Simple data structures
 * with POD members don't need to implement this.
 * 
 * When an object implements this interface, it will be serialized to the
 * object returned by adapt() before being transmitted as the result of a
 * command. This will be transmitted as result in REST resources and printed
 * after command execution.
 * 
 * @author ferdinando.villa
 *
 */
public interface IRemoteSerializable {

    /**
     * Use this key in maps to reference the most general representative interface for
     * the result. This should be the simpleName() of an API interface visible at all sides.
     */
    String RESULT_TYPE_KEY = "result-type";

    /**
     * Return the object that we want to use as the return value. This
     * should be readily serializable to JSON - i.e. it should be a simple
     * POD value, or a list/map with simple members.
     * 
     * @return
     */
    Object adapt();
}
