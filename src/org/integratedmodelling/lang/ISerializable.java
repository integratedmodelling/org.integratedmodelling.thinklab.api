package org.integratedmodelling.lang;

import java.io.Serializable;

/**
 * An ISerializable, in addition to be a Java Serializable, also implements a 
 * fromString(String) method so that its toString() value can be converted back
 * to an object. The method should be implemented with the return type of the
 * object itself.
 * 
 * @author Ferd
 *
 */
public interface ISerializable extends Serializable {

    ISerializable fromString(String stringRep);

}
