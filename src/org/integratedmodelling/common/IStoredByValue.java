package org.integratedmodelling.common;

/**
 * Tag interface used to mark object that should be seen as complex literals. Such objects
 * should compare and hash according to their literal "value" and not as individual objects.
 * This is used, at the moment, to distinguish objects whose semantics should be copied in
 * storage rather than storing multiple references to different objects that return the same hash code.
 * 
 * @author Ferd
 *
 */
public interface IStoredByValue {

}
